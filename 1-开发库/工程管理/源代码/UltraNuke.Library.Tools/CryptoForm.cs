﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UltraNuke.Cryptography;

namespace UltraNuke.Library.Tools
{
    public partial class CryptoForm : Form
    {
        public CryptoForm()
        {
            InitializeComponent();
        }

        private void LogForm_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ICrypto c = CryptoFactory.Create();
            textBox2.Text = c.Encrypt(textBox1.Text, (CryptoMode)Enum.Parse(typeof(CryptoMode), comboBox1.Text));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;
        }
    }
}
