﻿namespace UltraNuke.Library.Tools
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.加密 = new System.Windows.Forms.Button();
            this.SQLite加密 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // 加密
            // 
            this.加密.Location = new System.Drawing.Point(12, 12);
            this.加密.Name = "加密";
            this.加密.Size = new System.Drawing.Size(58, 43);
            this.加密.TabIndex = 0;
            this.加密.Text = "加密";
            this.加密.UseVisualStyleBackColor = true;
            this.加密.Click += new System.EventHandler(this.加密_Click);
            // 
            // SQLite加密
            // 
            this.SQLite加密.Location = new System.Drawing.Point(92, 12);
            this.SQLite加密.Name = "SQLite加密";
            this.SQLite加密.Size = new System.Drawing.Size(75, 43);
            this.SQLite加密.TabIndex = 1;
            this.SQLite加密.Text = "SQLite加密";
            this.SQLite加密.UseVisualStyleBackColor = true;
            this.SQLite加密.Click += new System.EventHandler(this.SQLite加密_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 305);
            this.Controls.Add(this.SQLite加密);
            this.Controls.Add(this.加密);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button 加密;
        private System.Windows.Forms.Button SQLite加密;

    }
}

