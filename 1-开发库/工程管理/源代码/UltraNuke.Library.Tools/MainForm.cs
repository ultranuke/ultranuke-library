﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UltraNuke.Library.Tools
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void 加密_Click(object sender, EventArgs e)
        {
            CryptoForm c = new CryptoForm();
            c.Show();
        }

        private void SQLite加密_Click(object sender, EventArgs e)
        {
            SQLitePasswordForm c = new SQLitePasswordForm();
            c.Show();
        }
    }
}
