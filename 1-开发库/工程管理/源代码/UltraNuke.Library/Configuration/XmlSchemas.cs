﻿using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;

namespace UltraNuke.Configuration
{
    /// <summary>
    /// 配置相关的XSD架构的定义
    /// </summary>
    internal class XmlSchemas
    {

        #region 公共方法
        public XmlReaderSettings CreateConfigReaderSettings()
        {
            XmlReaderSettings result = CreateXmlReaderSettings(Config);
            result.ValidationEventHandler += new ValidationEventHandler(ConfigSettingsValidationEventHandler);
            result.IgnoreComments = true;
            return result;
        }
        #endregion

        #region 字段
        private readonly XmlSchema Config = ReadXmlSchemaFromEmbeddedResource(CfgSchemaResource);
        #endregion

        #region 常量
        private const string CfgSchemaResource = "UltraNuke.Library.Configuration.Cfg.xsd";
        #endregion

        #region 方法
        private static XmlSchema ReadXmlSchemaFromEmbeddedResource(string resourceName)
        {
            Assembly executingAssembly = Assembly.GetExecutingAssembly();

            using (Stream resourceStream = executingAssembly.GetManifestResourceStream(resourceName))
                return XmlSchema.Read(resourceStream, null);
        }

        private static XmlReaderSettings CreateXmlReaderSettings(XmlSchema xmlSchema)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(xmlSchema);
            return settings;
        }
        #endregion

        #region 事件
        private static void ConfigSettingsValidationEventHandler(object sender, ValidationEventArgs e)
        {
            throw new CfgException("配置解析发生异常:" + e.Message, e.Exception);
        }
        #endregion
    }
}
