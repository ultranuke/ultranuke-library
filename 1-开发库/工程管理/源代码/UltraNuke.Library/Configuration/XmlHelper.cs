﻿using System.Xml;
using System.Xml.XPath;

namespace UltraNuke.Configuration
{
    /// <summary>
    /// 配置相关的XML帮助器
    /// </summary>
    public static class XmlHelper
    {
        #region 初始化
        static XmlHelper()
        {
            NameTable nt = new NameTable();
            XmlNamespaceManager nsMgr = new XmlNamespaceManager(nt);
            nsMgr.AddNamespace(NamespacePrefix, SchemaXmlns);

            PropertiesExpression = XPathExpression.Compile(RootPrefixPath + "properties", nsMgr);
            PropertiesPropertyExpression = XPathExpression.Compile(RootPrefixPath + "properties/" + ChildPrefixPath + "property", nsMgr);
        }
        #endregion

        #region 公共字段
        public static readonly XPathExpression PropertiesPropertyExpression;
        public static readonly XPathExpression PropertiesExpression;
        #endregion

        #region 公共常量
        public const string SectionName = "ultranuke";
        public const string SchemaXmlns = "urn:ultranuke";
        public const string NamespacePrefix = "cfg";
        #endregion

        #region 常量
        private const string RootPrefixPath = "//" + NamespacePrefix + ":";
        private const string ChildPrefixPath = NamespacePrefix + ":";
        #endregion

    }
}
