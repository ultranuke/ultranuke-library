﻿using System.Configuration;
using System.Xml;

namespace UltraNuke.Configuration
{
    /// <summary>
    /// 处理对特定的配置节的访问
    /// </summary>
    public sealed class CfgSectionHandler : IConfigurationSectionHandler
    {
        object IConfigurationSectionHandler.Create(object parent, object configContext, XmlNode section)
        {
            return SimpleCfgRootManager.FromAppConfig(section);
        }
    }
}
