﻿using System;
using System.Xml;
using System.Xml.XPath;

namespace UltraNuke.Configuration
{
    /// <summary>
    /// 提供应用程序配置操作
    /// </summary>
    public class SimpleCfgRootManager
    {
        #region 初始化
        private SimpleCfgRootManager(XmlReader configurationReader, bool fromAppSetting)
        {
            XPathNavigator nav;
            try
            {
                nav = new XPathDocument(XmlReader.Create(configurationReader, (new XmlSchemas()).CreateConfigReaderSettings())).CreateNavigator();

            }
            catch (CfgException)
            {
                throw;
            }
            catch (Exception ex)
            {
                CfgException e = new CfgException("发生配置错误！", ex);
                throw e;
            }

            Parse(nav, fromAppSetting);
        }
        #endregion

        #region 方法
        private void Parse(XPathNavigator navigator, bool fromAppConfig)
        {
            XPathNavigator xpn = navigator.SelectSingleNode(XmlHelper.PropertiesExpression);
            if (xpn != null)
            {
                propertiesManager = new SimpleCfgPropertiesManager(navigator);
            }
            else
            {
                if (!fromAppConfig)
                {
                    CfgException e = new CfgException("配置文件中没有发现标签<properties xmlns='" + XmlHelper.SchemaXmlns + "'>。");
                    throw e;
                }
            }
        }

        internal static SimpleCfgRootManager FromAppConfig(XmlNode node)
        {
            XmlTextReader reader = new XmlTextReader(node.OuterXml, XmlNodeType.Document, null);
            return new SimpleCfgRootManager(reader, true);
        }
        #endregion

        #region 字段
        private SimpleCfgPropertiesManager propertiesManager;
        /// <summary>
        /// 属性集合
        /// </summary>
        public SimpleCfgPropertiesManager PropertiesManager
        {
            set { propertiesManager = value; }
            get { return propertiesManager; }
        }
        #endregion
    }
}
