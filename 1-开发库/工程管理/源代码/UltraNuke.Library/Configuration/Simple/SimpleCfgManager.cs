﻿using System.Collections.Generic;
using System.Configuration;

namespace UltraNuke.Configuration
{
    /// <summary>
    /// 提供应用程序配置操作
    /// </summary>
    public class SimpleCfgManager : ICfg
    {
        #region 初始化
        private SimpleCfgManager()
        {
            properties = new Dictionary<string, string>();
            LoadGlobalPropertiesFromAppConfig();
        }

        public static SimpleCfgManager CfgManager
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObject)
                    {
                        if (instance == null)
                        {
                            instance = new SimpleCfgManager();
                        }
                    }
                }
                return instance;
            }
        }
        #endregion

        #region 公共属性
        private IDictionary<string, string> properties;
        /// <summary>
        /// 属性集合
        /// </summary>
        public IDictionary<string, string> Properties
        {
            get { return properties; }
        }
        #endregion

        #region 方法
        private void LoadGlobalPropertiesFromAppConfig()
        {
            SimpleCfgRootManager configurationManager = ConfigurationManager.GetSection(XmlHelper.SectionName) as SimpleCfgRootManager;

            foreach (KeyValuePair<string, string> kvp in configurationManager.PropertiesManager.Properties)
            {
                properties[kvp.Key] = kvp.Value;
            }
        }
        #endregion

        #region 字段
        private static SimpleCfgManager instance;
        private static object lockObject = new object();
        #endregion
    }
}
