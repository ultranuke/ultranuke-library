﻿using System;
using System.Collections.Generic;
using System.Xml.XPath;

namespace UltraNuke.Configuration
{
    /// <summary>
    /// 配置类工厂
    /// </summary>
    public class SimpleCfgPropertiesManager
    {
        #region 初始化
        internal SimpleCfgPropertiesManager(XPathNavigator configurationSection)
        {
            if (configurationSection == null)
                throw new ArgumentNullException("configurationSection");

            Parse(configurationSection);
        }
        #endregion

        #region 公共属性
        private IDictionary<string, string> properties = new Dictionary<string, string>();
        /// <summary>
        /// 属性集合
        /// </summary>
        public IDictionary<string, string> Properties
        {
            get { return properties; }
        }
        #endregion

        #region 方法
        private void Parse(XPathNavigator navigator)
        {
            ParseProperties(navigator);
        }

        private void ParseProperties(XPathNavigator navigator)
        {
            XPathNodeIterator xpni = navigator.Select(XmlHelper.PropertiesPropertyExpression);
            while (xpni.MoveNext())
            {
                string propName;
                string propValue;
                if (xpni.Current.Value != null)
                {
                    propValue = xpni.Current.Value.Trim();
                }
                else
                {
                    propValue = string.Empty;
                }
                XPathNavigator pNav = xpni.Current.Clone();
                pNav.MoveToFirstAttribute();
                propName = pNav.Value;
                if (!string.IsNullOrEmpty(propName))
                    properties[propName] = propValue;
            }
        }
        #endregion

    }
}
