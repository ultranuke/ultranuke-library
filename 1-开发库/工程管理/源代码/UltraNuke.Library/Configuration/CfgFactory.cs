﻿using UltraNuke.Library.Properties;

namespace UltraNuke.Configuration
{
    /// <summary>
    /// 配置工厂类
    /// </summary>
    public sealed class CfgFactory
    {
        private CfgFactory()
        {            
        }
        
        /// <summary>
        /// 创建工厂实例
        /// </summary>
        /// <returns>配置接口</returns>
        public static ICfg Create()
        {
            return Create(Resources.DefaultConfigurationMananger);
        }

        /// <summary>
        /// 创建工厂实例
        /// </summary>
        /// <param name="cfgManager">配置管理者</param>
        /// <returns>配置接口</returns>
        public static ICfg Create(string cfgManager)
        {
            ICfg cfg;
            switch (cfgManager.ToLower())
            {
                case "simpleconfiguration":
                    cfg = SimpleCfgManager.CfgManager;
                    break;
                default:
                    throw new CfgException("没有具体的缓存管理者！");
            }
            return cfg;
        }
    }
}
