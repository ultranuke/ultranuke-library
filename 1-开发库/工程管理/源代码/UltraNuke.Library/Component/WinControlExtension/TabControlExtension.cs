﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UltraNuke.Library.Properties;
using System.Drawing.Drawing2D;
using System.Drawing.Text;

namespace UltraNuke.Component.WinControlExtension
{
    /// <summary>
    /// 扩展的TabControl控件。
    /// </summary>
    public partial class TabControlExtension : TabControl
    {
        #region 初始化
        public TabControlExtension()
        {
            InitializeComponent();
            SetStyles();
        }
        #endregion

        #region 公共属性
        private Image skinMainBackGround = Resources.tab_background;
        /// <summary>
        /// TabContrl控件主体区域背景图片
        /// </summary>
        [CategoryAttribute("Appearance"), Description("TabContrl控件主体区域背景图片")]
        public Image SkinMainBackGround
        {
            get { return this.skinMainBackGround; }
            set
            {
                skinMainBackGround = value;
                this.Invalidate();
            }
        }

        private Image skinHeaderBackGround = Resources.tab_headerbackground;
        /// <summary>
        /// TabContrl控件头部区域背景图片
        /// </summary>
        [CategoryAttribute("Appearance"), Description("TabContrl控件头部区域背景图片")]
        public Image SkinHeaderBackGround
        {
            get { return this.skinHeaderBackGround; }
            set
            {
                skinHeaderBackGround = value;
                this.Invalidate();
            }
        }

        private Image skinTabNormalImage = Resources.tab_normal;
        /// <summary>
        /// TabContrl控件选项卡默认状态的图片
        /// </summary>
        [CategoryAttribute("Appearance"), Description("TabContrl控件选项卡鼠标移到按钮上的图片")]
        public Image SkinTabNormalImage
        {
            get { return this.skinTabNormalImage; }
            set
            {
                skinTabNormalImage = value;
                this.Invalidate();
            }
        }

        private Image skinTabHoverImage = Resources.tab_hover;
        /// <summary>
        /// 鼠标移到TabContrl控件选项卡上的图片
        /// </summary>
        [CategoryAttribute("Appearance"), Description("鼠标移到TabContrl控件选项卡上的图片")]
        public Image SkinTabHoverImage
        {
            get { return this.skinTabHoverImage; }
            set
            {
                skinTabHoverImage = value;
                this.Invalidate();
            }
        }

        private Image skinTabPushedImage = Resources.tab_pushed;
        /// <summary>
        /// TabContrl控件选项卡选中状态的图片
        /// </summary>
        [CategoryAttribute("Appearance"), Description("TabContrl控件选项卡选中状态的图片")]
        public Image SkinTabPushedImage
        {
            get { return this.skinTabPushedImage; }
            set
            {
                skinTabPushedImage = value;
                this.Invalidate();
            }
        }

        private Color skinBorderColor = Color.FromArgb(185, 185, 185);
        /// <summary>
        /// TabContrl控件的边框颜色
        /// </summary>
        [CategoryAttribute("Appearance"), Description("TabContrl控件的边框颜色")]
        public Color SkinBorderColor
        {
            get { return this.skinBorderColor; }
            set
            {
                skinBorderColor = value;
                this.Invalidate();
            }
        }
        #endregion

        #region 重写方法
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            DrawTabContrl(e.Graphics);
        }
        #endregion

        #region 方法
        /// <summary>
        /// 指定控件的样式和行为。
        /// </summary>
        private void SetStyles()
        {
            base.SetStyle(
                ControlStyles.UserPaint |
                ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.ResizeRedraw |
                ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.StandardDoubleClick, false);
            this.BackColor = Color.Transparent;
            base.UpdateStyles();
        }
        
        #region 绘制TabContrl控件方法
        /// <summary>
        /// 绘制TabContrl控件
        /// </summary>
        /// <param name="g">绘制对象</param>
        private void DrawTabContrl(Graphics g)
        {
            DrawTabControlBackground(g);
            DrawTabPages(g);
            DrawBorder(g);
        }

        /// <summary>
        /// 绘制TabContrl控件的背景
        /// </summary>
        /// <param name="g">Graphics对象</param>
        private void DrawTabControlBackground(Graphics g)
        {
            int x = 0;
            int y = 0;
            int width = ClientRectangle.Width;
            int height = ClientRectangle.Height - DisplayRectangle.Height;

            Rectangle headerRect = new Rectangle(x, y, width, height);
            g.DrawImage(skinMainBackGround, ClientRectangle);
            g.DrawImage(skinHeaderBackGround, headerRect);

        }

        /// <summary>
        /// 绘制TabContrl控件的选项卡
        /// </summary>
        /// <param name="g">Graphics对象</param>
        private void DrawTabPages(Graphics g)
        {
            Image image = null;
            Rectangle tabRect;
            Point cusorPoint = PointToClient(MousePosition);
            bool hover;
            bool selected;

            for (int i = 0; i < base.TabCount; i++)
            {
                TabPage page = TabPages[i];

                tabRect = GetTabRect(i);
                hover = tabRect.Contains(cusorPoint);
                selected = SelectedIndex == i;

                if (selected)
                {
                    image = skinTabPushedImage;
                }
                else if (hover)
                {
                    image = skinTabHoverImage;
                }
                else
                {
                    image = skinTabNormalImage;
                }

                g.DrawImage(image, tabRect);

                TextRenderer.DrawText(
                    g,
                    page.Text,
                    page.Font,
                    tabRect,
                    page.ForeColor);
            }
        }

        /// <summary>
        /// 绘制TabContrl控件的边框
        /// </summary>
        /// <param name="g">Graphics对象</param>
        private void DrawBorder(Graphics g)
        {
            if (SelectedIndex != -1)
            {
                Rectangle tabRect = GetTabRect(SelectedIndex);
                Rectangle clipRect = ClientRectangle;
                Point[] points = new Point[6];
                points[0] = new Point(
                                tabRect.X,
                                tabRect.Bottom);
                points[1] = new Point(
                    clipRect.X,
                    tabRect.Bottom);
                points[2] = new Point(
                    clipRect.X,
                    clipRect.Bottom - 1);
                points[3] = new Point(
                    clipRect.Right - 1,
                    clipRect.Bottom - 1);
                points[4] = new Point(
                    clipRect.Right - 1,
                    tabRect.Bottom);
                points[5] = new Point(
                    tabRect.Right,
                    tabRect.Bottom);

                using (Pen pen = new Pen(skinBorderColor))
                {
                    g.DrawLines(pen, points);
                }
            }
        }
        #endregion
        #endregion        

    }
}
