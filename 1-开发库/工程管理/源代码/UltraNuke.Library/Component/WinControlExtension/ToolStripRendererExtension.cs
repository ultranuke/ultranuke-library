﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using UltraNuke.Library.Properties;

namespace UltraNuke.Component.WinControlExtension
{
    /// <summary>
    /// 自定义 ToolStrip 控件的外观
    /// </summary>
    public class ToolStripRendererExtension : ToolStripRenderer
    {
        #region 重写方法
        protected override void OnRenderButtonBackground(ToolStripItemRenderEventArgs e)
        {            
            Bitmap image;
            bool isChecked = (e.Item as ToolStripButton).Checked;
            if (e.Item.Selected && !isChecked)
            {
                image = Resources.toolbar_hover;
            }
            else if (isChecked)
            {
                image = Resources.toolbar_pushed;
            }
            else
            {
                image = Resources.toolbar_normal;
            }
            Rectangle rc = new Rectangle(Point.Empty,e.Item.Size);
            Graphics g = e.Graphics;
            g.DrawImage(image, rc);
        }
        #endregion
    }
}
