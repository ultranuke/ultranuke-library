﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UltraNuke.Library.Properties;

namespace UltraNuke.Component.WinControlExtension
{
    /// <summary>
    /// 扩展的Button控件。
    /// </summary>
    public partial class ButtonExtension : Button
    {
        #region 初始化
        public ButtonExtension()
        {
            InitializeComponent();
            SetStyles();
        }
        #endregion

        #region 公共属性
        private Image skinNormalImage = Resources.button_normal;
        /// <summary>
        /// 按钮默认状态的图片
        /// </summary>
        [CategoryAttribute("Appearance"), Description("按钮默认状态的图片")]
        public Image SkinNormalImage
        {
            get { return this.skinNormalImage; }
            set
            {
                skinNormalImage = value;
                this.Invalidate();
            }
        }

        private Image skinHoverImage = Resources.button_hover;
        /// <summary>
        /// 鼠标移到按钮上的图片
        /// </summary>
        [CategoryAttribute("Appearance"), Description("鼠标移到按钮上的图片")]
        public Image SkinHoverImage
        {
            get { return this.skinHoverImage; }
            set
            {
                skinHoverImage = value;
                this.Invalidate();
            }
        }

        private Image skinPushedImage = Resources.button_pushed;
        /// <summary>
        /// 鼠标按下按钮时的图片
        /// </summary>
        [CategoryAttribute("Appearance"), Description("鼠标按下按钮时的图片")]
        public Image SkinPushedImage
        {
            get { return this.skinPushedImage; }
            set
            {
                skinPushedImage = value;
                this.Invalidate();
            }
        }

        private Image skinDisableImage = Resources.button_disable;
        /// <summary>
        /// 按钮禁用状态时的图片
        /// </summary>
        [CategoryAttribute("Appearance"), Description("按钮禁用状态时的图片")]
        public Image SkinDisableImage
        {
            get { return this.skinDisableImage; }
            set
            {
                skinDisableImage = value;
                this.Invalidate();
            }
        }

        private Image skinFocusImage = Resources.button_focus;
        /// <summary>
        /// 按钮得到Tab焦点时的图片
        /// </summary>
        [CategoryAttribute("Appearance"), Description("按钮得到Tab焦点时的图片")]
        public Image SkinFocusImage
        {
            get { return this.skinFocusImage; }
            set
            {
                skinFocusImage = value;
                this.Invalidate();
            }
        }
        #endregion

        #region 重写方法
        protected override void OnMouseEnter(EventArgs e)
        {
            state = State.Hover;
            this.Invalidate();
            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            state = State.Normal;
            this.Invalidate();
            base.OnMouseLeave(e);
        }

        protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) != MouseButtons.Left) return;

            state = State.Pushed;
            this.Invalidate();
            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(System.Windows.Forms.MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
                state = State.Normal;

            this.Invalidate();
            base.OnMouseUp(e);
        }

        protected override void OnLostFocus(EventArgs e)
        {
            state = State.Normal;

            this.Invalidate();
            base.OnLostFocus(e);
        }

        protected override void OnGotFocus(EventArgs e)
        {
            state = State.Focus;

            this.Invalidate();
            base.OnLostFocus(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            base.InvokePaintBackground(this, new PaintEventArgs(e.Graphics, base.ClientRectangle));

            DrawButtonContrl(e.Graphics);
        }
        #endregion

        #region 枚举
        /// <summary>
        /// 按钮的状态
        /// </summary>
        private enum State
        {
            Normal = 1,
            Hover = 2,
            Pushed = 3,
            Disable = 4,
            Focus = 5
        }
        #endregion

        #region 字段
        private State state = State.Normal;
        #endregion

        #region 方法
        /// <summary>
        /// 指定控件的样式和行为。
        /// </summary>
        private void SetStyles()
        {
            base.SetStyle(
                ControlStyles.UserPaint |
                ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.ResizeRedraw |
                ControlStyles.Selectable |
                ControlStyles.SupportsTransparentBackColor, true);
            base.UpdateStyles();
        }
        #endregion

        #region 绘制Button控件方法
        /// <summary>
        /// 绘制Button控件
        /// </summary>
        /// <param name="g">绘制对象</param>
        private void DrawButtonContrl(Graphics g)
        {
            Image image = null;
            Rectangle tabRect = this.ClientRectangle;

            if (this.Focused && state != State.Pushed) state = State.Focus;
            if (!this.Enabled) state = State.Disable;

            switch (state)
            {
                case State.Normal:
                    image = skinNormalImage;
                    break;
                case State.Hover:
                    image = skinHoverImage;
                    break;
                case State.Pushed:
                    image = skinPushedImage;
                    break;
                case State.Disable:
                    image = skinDisableImage;
                    break;
                case State.Focus:
                    image = skinFocusImage;
                    break;
                default:
                    image = skinNormalImage;
                    break;
            }

            g.DrawImage(image, tabRect);

            TextRenderer.DrawText(
                    g,
                    base.Text,
                    base.Font,
                    tabRect,
                    base.ForeColor);
        }
        #endregion
    }
}
