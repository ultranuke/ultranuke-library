﻿using System;
using System.Data;
using log4net.Appender;
using log4net.Config;
using log4net.Layout;
using UltraNuke.Configuration;
using UltraNuke.Cryptography;

namespace UltraNuke.Logging
{
    /// <summary>
    /// Log4NetManager提供以log4net方式构建的应用程序日志操作
    /// </summary>
    public class Log4NetManager : ILog
    {
        #region fields
        private const string DefaultLoggerName = "UltraNuke.Logger";
        
        private static log4net.ILog log;
        private static Log4NetManager instance;
        private static object lockObject = new object();
        #endregion

        #region constructors
        private Log4NetManager()
            : this(DefaultLoggerName)
        {
        }

        private Log4NetManager(string name)
        {
            log = log4net.LogManager.GetLogger(name);
        }

        private Log4NetManager(Type type)
        {
            log = log4net.LogManager.GetLogger(type);
        }

        public static Log4NetManager GetLoggingManager()
        {

            if (instance == null)
            {
                lock (lockObject)
                {
                    if (instance == null)
                    {
                        instance = new Log4NetManager();
                    }
                }
            }
            return instance;
        }

        public static Log4NetManager GetLoggingManager(string name)
        {
            if (instance == null)
            {
                lock (lockObject)
                {
                    if (instance == null)
                    {
                        instance = new Log4NetManager(name);
                    }
                }
            }
            return instance;
        }

        public static Log4NetManager GetLoggingManager(Type type)
        {
            if (instance == null)
            {
                lock (lockObject)
                {
                    if (instance == null)
                    {
                        instance = new Log4NetManager(type);
                    }
                }
            }
            return instance;
        }
        #endregion

        #region methods
        /// <summary>
        /// 写入日志消息
        /// </summary>
        /// <param name="level">日志级别</param>
        /// <param name="message">日志消息</param>
        public void Write(string message, LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Debug:
                    if (log.IsDebugEnabled)
                        log.Debug(message);
                    break;
                case LogLevel.Info:
                    if (log.IsInfoEnabled)
                        log.Info(message);
                    break;
                case LogLevel.Warn:
                    if (log.IsWarnEnabled)
                        log.Warn(message);
                    break;
                case LogLevel.Error:
                    if (log.IsErrorEnabled)
                        log.Error(message);
                    break;
            }
        }

        /// <summary>
        /// 写入日志消息
        /// </summary>
        /// <param name="level">日志级别</param>
        /// <param name="message">日志消息</param>
        /// <param name="exception">异常对象</param>
        public void Write(string message, Exception exception, LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Debug:
                    if (log.IsDebugEnabled)
                        log.Debug(message, exception);
                    break;
                case LogLevel.Info:
                    if (log.IsInfoEnabled)
                        log.Info(message, exception);
                    break;
                case LogLevel.Warn:
                    if (log.IsWarnEnabled)
                        log.Warn(message, exception);
                    break;
                case LogLevel.Error:
                    if (log.IsErrorEnabled)
                        log.Error(message, exception);
                    break;
            }
        }
        #endregion
    }
}
