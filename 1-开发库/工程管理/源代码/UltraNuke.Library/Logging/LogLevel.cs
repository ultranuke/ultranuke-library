﻿
namespace UltraNuke.Logging
{
    /// <summary>
    /// 日志等级
    /// </summary>
    public enum LogLevel
    {
        Debug,
        Info,
        Warn,
        Error
    }
}
