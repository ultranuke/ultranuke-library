﻿using System;

namespace UltraNuke.Logging
{
    /// <summary>
    /// ILog提供应用程序日志操作的接口
    /// </summary>
    public interface ILog
    {
        /// <summary>
        /// 写入日志消息
        /// </summary>
        /// <param name="message">日志消息</param>
        /// <param name="level">日志级别</param>
        void Write(string message, LogLevel level);

        /// <summary>
        /// 写入日志消息
        /// </summary>
        /// <param name="message">日志消息</param>
        /// <param name="exception">异常对象</param>
        /// <param name="level">日志级别</param>
        void Write(string message, Exception exception, LogLevel level);
    }
}
