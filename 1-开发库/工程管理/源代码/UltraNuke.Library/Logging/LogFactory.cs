﻿using System;
using UltraNuke.Library.Properties;

namespace UltraNuke.Logging
{
    /// <summary>
    /// 日志工厂类
    /// </summary>
    public sealed class LogFactory
    {
        private LogFactory()
        {            
        }

        /// <summary>
        /// 通过名称创建实例
        /// </summary>
        /// <returns>日志接口</returns>
        public static ILog Create()
        {
            return Create(Resources.DefaultLoggingManager);
        }

        /// <summary>
        /// 通过名称创建实例
        /// </summary>
        /// <param name="loggingManager">日志管理者</param>
        /// <returns>日志接口</returns>
        public static ILog Create(string loggingManager)
        {
            ILog log;
            switch (loggingManager.ToLower())
            {
                case "log4net":
                    log = Log4NetManager.GetLoggingManager();
                    break;
                default:
                    throw new LogException("没有具体的日志管理者！");
            }
            return log;
        }

        /// <summary>
        /// 通过类型创建实例
        /// </summary>
        /// <param name="type">使用日志类的类型</param>
        /// <returns>日志接口</returns>
        public static ILog Create(Type type)
        {
            return Create(type, Resources.DefaultLoggingManager);
        }
        
        /// <summary>
        /// 通过类型创建实例
        /// </summary>
        /// <param name="type">使用日志类的类型</param>
        /// <param name="loggingManager">日志管理者</param>
        /// <returns>日志接口</returns>
        public static ILog Create(Type type, string loggingManager)
        {
            ILog log;
            switch (loggingManager.ToLower())
            {
                case "log4net":
                    log = Log4NetManager.GetLoggingManager(type);
                    break;
                default:
                    throw new LogException("没有具体的日志管理者！");
            }
            return log;
        }
    }
}
