﻿using AopAlliance.Intercept;

namespace UltraNuke.Logging
{
    public class LogAdvice: IMethodInterceptor
    {
        public object Invoke(IMethodInvocation invocation)
        {
            ILog log = LogFactory.Create(invocation.TargetType);
            object result = invocation.Proceed();
            log.Write(invocation.TargetType.Name + "." + invocation.Method.Name, LogLevel.Info);
            return result;
        }
    }
}
