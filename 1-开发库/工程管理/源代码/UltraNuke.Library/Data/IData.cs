﻿using System.Collections.Generic;

namespace UltraNuke.Data
{
    /// <summary>
    /// IData提供应用程序数据操作的接口
    /// </summary>
    public interface IData
    {
        /// <summary>
        /// 保存记录
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <returns>标识符</returns>
        object Save(object entity);

        /// <summary>
        /// 更新记录
        /// </summary>
        /// <param name="entity">实体对象</param>
        void Update(object entity);

        /// <summary>
        /// 删除记录
        /// </summary>
        /// <param name="entity">实体对象</param>
        void Delete(object entity);

        /// <summary>
        /// 获取记录
        /// </summary>
        /// <typeparam name="T">记录类型</typeparam>
        /// <param name="id">标识符</param>
        /// <returns>实体对象</returns>
        T Get<T>(object id);

        /// <summary>
        /// 获取记录集合
        /// </summary>
        /// <typeparam name="T">记录类型</typeparam>
        /// <returns>实体对象集合</returns>
        IList<T> Gets<T>();

        /// <summary>
        /// 清空实例
        /// </summary>
        void Clear();

        /// <summary>
        /// 清理缓存,执行sql
        /// </summary>
        void Flush();

        /// <summary>
        /// 释放资源
        /// </summary>
        void Dispose();

        /// <summary>
        /// 开始事务
        /// </summary>
        void BeginTransaction();

        /// <summary>
        /// 提交事务
        /// </summary>
        void Commit();

        /// <summary>
        /// 回滚事务
        /// </summary>
        void Rollback();
    }
}
