using System;
using System.Collections.Generic;

namespace UltraNuke.Data
{
    /// <summary>
    /// IData<T>使用泛型方式提供应用程序数据操作的接口
    /// </summary>
	/// <typeparam name="T">实体类型</typeparam>
    public interface IData<T>
    {
        /// <summary>
        /// 新增记录
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns>标识符</returns>
        object Save(T entity);

        /// <summary>
        /// 更新记录
        /// </summary>
        /// <param name="entity">实体</param>
        void Update(T entity);

        /// <summary>
        /// 删除记录
        /// </summary>
        /// <param name="entity">实体</param>
        void Delete(T entity);

        /// <summary>
        /// 删除记录
        /// </summary>
        /// <param name="id">标识</param>
        void Delete(Guid id);

		/// <summary>
        /// 获取记录
        /// </summary>
        /// <returns>记录</returns>
        T Get(object id);

		/// <summary>
		/// 获取记录集合
		/// </summary>
		/// <returns>记录集合</returns>
        IList<T> Gets();
    }
}
