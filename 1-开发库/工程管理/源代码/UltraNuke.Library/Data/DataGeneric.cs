using System;
using System.Collections.Generic;
using UltraNuke.Logging;

namespace UltraNuke.Data
{
	/// <summary>
    /// Data<T>使用泛型方式提供应用程序数据操作
	/// </summary>
	/// <typeparam name="T">实体类型</typeparam>
    public class Data<T> : IData<T>
	{
        /// <summary>
        /// 保存记录
        /// </summary>
        /// <param name="entity">实体</param>
        public virtual object Save(T entity)
        {
            IData session = DataFactory.Create();
            try
            {
                session.BeginTransaction();
                object id = session.Save(entity);
                session.Commit();
                return id;
            }
            catch (Exception ex)
            {
                session.Rollback();
                DataException e = new DataException("保存记录发生错误！", ex);
                throw e;
            }
            finally
            {
                session.Dispose();
            }
        }

		/// <summary>
		/// 更新记录
		/// </summary>
        /// <param name="entity">实体</param>
        public virtual void Update(T entity)
        {
            IData session = DataFactory.Create();
            try
            {
                session.BeginTransaction();
                session.Clear();
                session.Update(entity);
                session.Commit();
            }
            catch (Exception ex)
            {
                session.Rollback();
                DataException e = new DataException("更新记录发生错误！", ex);
                throw e;
            }
            finally
            {
                session.Dispose();
            }

		}

		/// <summary>
		/// 删除记录
		/// </summary>
        /// <param name="entity">实体</param>
        public virtual void Delete(T entity)
        {
            IData session = DataFactory.Create();
            try
            {
                session.BeginTransaction();
                session.Clear();
                session.Delete(entity);
                session.Commit();
            }
            catch (Exception ex)
            {
                session.Rollback();
                DataException e = new DataException("删除记录发生错误！", ex); 
                throw e;
            }
            finally
            {
                session.Dispose();
            }
		}

        /// <summary>
        /// 删除记录
        /// </summary>
        /// <param name="id">标识</param>
        public virtual void Delete(Guid id)
        {
            IData session = DataFactory.Create();
            try
            {
                T t = (T)session.Get<T>(id);

                session.BeginTransaction();
                session.Clear(); 
                session.Delete(t);
                session.Commit();
            }
            catch (Exception ex)
            {
                session.Rollback();
                DataException e = new DataException("删除记录发生错误！", ex);
                throw e;
            }
            finally
            {
                session.Dispose();
            }
        }

		/// <summary>
		/// 获取记录信息
		/// </summary>
		/// <param name="id">主键</param>
		/// <returns>记录</returns>
		public virtual T Get(object id)
        {
            IData session = DataFactory.Create();
            try
            {
                T t = (T)session.Get<T>(id);
                return t;
            }
            catch (Exception ex)
            {
                DataException e = new DataException("获取记录信息发生错误！", ex);
                throw e;
            }
		}

		/// <summary>
		/// 获取记录集合
		/// </summary>
		/// <returns>记录集合</returns>
		public IList<T> Gets()
		{
            IData session = DataFactory.Create();
            try
            {
                IList<T> list = session.Gets<T>();
                return list;
            }
            catch (Exception ex)
            {
                DataException e = new DataException("获取记录集合发生错误！", ex);
                throw e;
            }
        }
	}
}
