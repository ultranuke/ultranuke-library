﻿using System.Collections.Generic;
using NHibernate;

namespace UltraNuke.Data
{
    /// <summary>
    /// NHibernateManager提供以NHibernate方式构建的应用程序数据操作
    /// </summary>
    public class NHibernateManager : IData
    {
        private ISession session;
        private ITransaction trans = null;
        public NHibernateManager()
        {
            session = NHibernateUtils.Session;
        }

        /// <summary>
        /// 保存记录
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <returns>标识符</returns>
        public object Save(object entity)
        {
            object id = session.Save(entity);
            return id;
        }

        /// <summary>
        /// 更新记录
        /// </summary>
        /// <param name="entity">实体对象</param>
        public void Update(object entity)
        {
            session.Update(entity);
        }

        /// <summary>
        /// 删除记录
        /// </summary>
        /// <param name="entity">实体对象</param>
        public void Delete(object entity)
        {
            session.Delete(entity);
        }

        /// <summary>
        /// 获取记录
        /// </summary>
        /// <typeparam name="T">记录类型</typeparam>
        /// <param name="id">标识符</param>
        /// <returns>实体对象</returns>
        public T Get<T>(object id)
        {
            T t = (T)session.Get<T>(id);
            return t;
        }

        /// <summary>
        /// 获取记录集合
        /// </summary>
        /// <typeparam name="T">记录类型</typeparam>
        /// <returns>实体对象集合</returns>
        public IList<T> Gets<T>()
        {
            IList<T> list = session.CreateCriteria(typeof(T)).List<T>();
            return list;
        }

        /// <summary>
        /// 清空实例
        /// </summary>
        public void Clear()
        {
            session.Clear();
        }

        /// <summary>
        /// 清理缓存,执行sql
        /// </summary>
        public void Flush()
        {
            session.Flush();
        }

        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose()
        {
            NHibernateUtils.CloseSession();
        }

        /// <summary>
        /// 开始事务
        /// </summary>
        public void BeginTransaction()
        {
            trans = session.BeginTransaction();
        }

        /// <summary>
        /// 提交事务
        /// </summary>
        public void Commit()
        {
            trans.Commit();
        }

        /// <summary>
        /// 回滚事务
        /// </summary>
        public void Rollback()
        {
            if (trans != null) trans.Rollback();
        }
    }
}
