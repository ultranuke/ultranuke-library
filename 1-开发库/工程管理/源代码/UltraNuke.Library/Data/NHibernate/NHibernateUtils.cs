using NHibernate;
using UltraNuke.Caching;
using UltraNuke.Configuration;
using UltraNuke.Cryptography;

namespace UltraNuke.Data
{
	/// <summary>
    /// NHibernateUtils是一个工具类，它提供的NHibernate操作的实用方法
	/// </summary>
    public sealed class NHibernateUtils
    {
        private static object locker = new object();
        private static ISessionFactory sessionFactory;
        private readonly static ICrypto Crypto = CryptoFactory.Create();
        private readonly static ICfg Cfg = CfgFactory.Create();
        private readonly static ICache Cache = CacheFactory.Create();
        
        private NHibernateUtils()
        {
            
        }

        /// <summary>
        /// NHibernate的对象工厂
        /// </summary>
        internal static ISessionFactory SessionFactory
        {
            get
            {
                if (sessionFactory == null)
                {
                    lock (locker)
                    {
                        if (sessionFactory == null)
                        {

                            NHibernate.Cfg.Configuration sessionConfiguration = new NHibernate.Cfg.Configuration();
                            SetProperties(sessionConfiguration);
                            sessionFactory = sessionConfiguration.Configure().BuildSessionFactory();
                        }
                    }
                }
                return sessionFactory;
            }
        }

        private static void SetProperties(NHibernate.Cfg.Configuration sessionConfiguration)
        {    
            if(!sessionConfiguration.Properties.ContainsKey("connection.connection_string"))
                sessionConfiguration.Properties.Add("connection.connection_string", Crypto.Decrypt(Cfg.Properties["data.ConnectionString"]));
        }

		/// <summary>
        /// 获取当前Session
		/// </summary>
        public static ISession Session
        {
            get
            {
                ISession s = (ISession)Cache.GetData("hibernate-isession");
                if (s == null)
                {
                    s = SessionFactory.OpenSession();
                    Cache.Add("hibernate-isession",s);
                }
                return s;
            }
        }

		/// <summary>
		/// 关闭Session
		/// </summary>
        public static void CloseSession()
        {
            ISession s = (ISession)Cache.GetData("hibernate-isession");
            if (s != null)
            {
                Cache.Remove("hibernate-isession");
                s.Close();
            }
        }
    }
}
