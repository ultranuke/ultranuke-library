﻿using UltraNuke.Library.Properties;

namespace UltraNuke.Data
{
    /// <summary>
    /// 数据处理工厂类
    /// </summary>
    public sealed class DataFactory
    {
        private DataFactory()
        {            
        }

        /// <summary>
        /// 创建工厂实例
        /// </summary>
        /// <returns>数据接口</returns>
        public static IData Create()
        {
            return Create(Resources.DefaultDataManager);
        }

        /// <summary>
        /// 创建工厂实例
        /// </summary>
        /// <param name="dataManager">数据管理者</param>
        /// <returns>数据接口</returns>
        public static IData Create(string dataManager)
        {
            IData data;
            switch (dataManager.ToLower())
            {
                case "nhibernate":
                    data = new NHibernateManager();
                    break;
                default:
                    throw new DataException("没有具体的数据管理者！");
            }
            return data;
        }
    }
}
