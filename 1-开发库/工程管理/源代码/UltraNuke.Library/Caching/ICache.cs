﻿
namespace UltraNuke.Caching
{
    /// <summary>
    /// ICache提供应用程序缓存操作的接口
    /// </summary>
    public interface ICache
    {
        /// <summary>
        /// 将指定项添加到 Cache 对象
        /// </summary>
        /// <param name="key">用于引用该项的缓存键</param>
        /// <param name="value">要添加到缓存的项</param>
        void Add(string key, object value);

        /// <summary>
        /// 确定缓存列表是否包含特定的缓存项。
        /// </summary>
        /// <param name="key">表示缓存项的键的 String 对象</param>
        /// <returns>如果在缓存列表中找到该缓存项，则为 true；否则为 false</returns>
        bool Contains(string key);

        /// <summary>
        /// 获取存储在缓存中的项数。
        /// </summary>
        int Count { get; }

        /// <summary>
        /// 删除所有的缓存项
        /// </summary>
        void Flush();

        /// <summary>
        /// 从 Cache 对象检索指定项
        /// </summary>
        /// <param name="key">要检索的缓存项的标识符</param>
        /// <returns>检索到的缓存项，未找到该键时为 null</returns>
        object GetData(string key);

        /// <summary>
        /// 从应用程序的 Cache 对象移除指定项
        /// </summary>
        /// <param name="key">要移除的缓存项的 String 标识符</param>
        void Remove(string key);

        /// <summary>
        /// 获取或设置指定键处的缓存项
        /// </summary>
        /// <param name="key">表示缓存项的键的 String 对象</param>
        /// <returns>指定的缓存项</returns>
        object this[string key] { get; }
    }
}
