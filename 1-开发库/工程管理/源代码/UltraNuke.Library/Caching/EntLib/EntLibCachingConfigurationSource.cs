﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Caching.BackingStoreImplementations;
using Microsoft.Practices.EnterpriseLibrary.Caching.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace UltraNuke.Caching
{
    public class EntLibCachingConfigurationSource : IConfigurationSource
    {
        #region IConfigurationSource 成员

        public void Add(IConfigurationParameter saveParameter, string sectionName, System.Configuration.ConfigurationSection configurationSection)
        {
            throw new NotImplementedException();
        }

        public void AddSectionChangeHandler(string sectionName, ConfigurationChangedEventHandler handler)
        {
            throw new NotImplementedException();
        }

        public System.Configuration.ConfigurationSection GetSection(string sectionName)
        {
            if (sectionName.Equals("cachingConfiguration", StringComparison.CurrentCulture))
            {
                CacheManagerSettings settings = new CacheManagerSettings();
                settings.DefaultCacheManager = "Default Cache Manager";
                CacheManagerData data = new CacheManagerData("Default Cache Manager", 60, 1000, 10, "inMemory");
                settings.CacheManagers.Add(data);
                CacheStorageData storageData = new CacheStorageData("inMemory", typeof(NullBackingStore));
                settings.BackingStores.Add(storageData);
                return settings;
            }
            return null;
        }

        public void Remove(IConfigurationParameter removeParameter, string sectionName)
        {
            throw new NotImplementedException();
        }

        public void RemoveSectionChangeHandler(string sectionName, ConfigurationChangedEventHandler handler)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
