﻿using Microsoft.Practices.EnterpriseLibrary.Caching;

namespace UltraNuke.Caching
{
    /// <summary>
    /// EntLibCachingManager提供以Microsoft.Practices.EnterpriseLibrary.Caching方式构建的应用程序缓存操作
    /// </summary>
    public class EntLibCachingManager : ICache
    {
        #region 初始化
        private EntLibCachingManager()
        {
            CacheManagerFactory factory = new CacheManagerFactory(new EntLibCachingConfigurationSource());
            cache = factory.CreateDefault();
        }

        public static EntLibCachingManager CachingManager
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObject)
                    {
                        if (instance == null)
                        {
                            instance = new EntLibCachingManager();
                        }
                    }
                }
                return instance;
            }
        }
        #endregion

        /// <summary>
        /// 将指定项添加到 Cache 对象
        /// </summary>
        /// <param name="key">用于引用该项的缓存键</param>
        /// <param name="value">要添加到缓存的项</param>
        public void Add(string key, object value)
        {
            cache.Add(key, value);
        }

        /// <summary>
        /// 确定缓存列表是否包含特定的缓存项。
        /// </summary>
        /// <param name="key">表示缓存项的键的 String 对象</param>
        /// <returns>如果在缓存列表中找到该缓存项，则为 true；否则为 false</returns>
        public bool Contains(string key)
        {
            return cache.Contains(key);
        }
        
        /// <summary>
        /// 获取存储在缓存中的项数。
        /// </summary>
        public int Count
        {
            get {
                return cache.Count;
            }
        }

        /// <summary>
        /// 删除所有的缓存项
        /// </summary>
        public void Flush()
        {
            cache.Flush();
        }

        /// <summary>
        /// 从 Cache 对象检索指定项
        /// </summary>
        /// <param name="key">要检索的缓存项的标识符</param>
        /// <returns>检索到的缓存项，未找到该键时为 null</returns>
        public object GetData(string key)
        {
            return cache.GetData(key);
        }

        /// <summary>
        /// 从应用程序的 Cache 对象移除指定项
        /// </summary>
        /// <param name="key">要移除的缓存项的 String 标识符</param>
        public void Remove(string key)
        {
            cache.Remove(key);
        }
        
        /// <summary>
        /// 获取或设置指定键处的缓存项
        /// </summary>
        /// <param name="key">表示缓存项的键的 String 对象</param>
        /// <returns>指定的缓存项</returns>
        public object this[string key]
        {
            get
            {
                return cache[key];
            }
        }

        #region 字段
        private Microsoft.Practices.EnterpriseLibrary.Caching.ICacheManager cache;
        private static EntLibCachingManager instance;
        private static object lockObject = new object();
        #endregion
    }
}
