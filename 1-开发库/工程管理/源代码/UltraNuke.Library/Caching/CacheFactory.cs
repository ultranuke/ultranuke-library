﻿using UltraNuke.Library.Properties;

namespace UltraNuke.Caching
{
    /// <summary>
    /// 缓存处理工厂类
    /// </summary>
    public sealed class CacheFactory
    {
        private CacheFactory()
        {            
        }
        
        /// <summary>
        /// 创建工厂实例
        /// </summary>
        /// <returns>缓存接口</returns>
        public static ICache Create()
        {
            return Create(Resources.DefaultCacheManager);
        }

        /// <summary>
        /// 创建工厂实例
        /// </summary>
        /// <param name="cacheManager">缓存管理者</param>
        /// <returns>缓存接口</returns>
        public static ICache Create(string cacheManager)
        {
            ICache cache;
            switch (cacheManager.ToLower())
            {
                case "microsoft.practices.enterpriselibrary.caching":
                    cache = EntLibCachingManager.CachingManager;
                    break;
                default:
                    throw new CacheException("没有具体的缓存管理者！");
            }
            return cache;
        }
    }
}
