﻿
namespace UltraNuke.Cryptography
{
    /// <summary>
    /// 加密方式
    /// </summary>
    public enum CryptoMode
    {
        DES,
        RC2,
        RSA,
        MD5
    }
}
