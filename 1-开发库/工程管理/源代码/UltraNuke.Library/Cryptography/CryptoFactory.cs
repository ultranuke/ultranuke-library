﻿using UltraNuke.Library.Properties;

namespace UltraNuke.Cryptography
{
    /// <summary>
    /// 加密工厂类
    /// </summary>
    public sealed class CryptoFactory
    {
        private CryptoFactory()
        {            
        }

        /// <summary>
        /// 通过名称创建实例
        /// </summary>
        /// <returns>加密接口</returns>
        public static ICrypto Create()
        {
            return Create(Resources.DefaultCryptographyManager);
        }

        /// <summary>
        /// 通过名称创建实例
        /// </summary>
        /// <param name="cryptographyManager">加密管理者</param>
        /// <returns>加密接口</returns>
        public static ICrypto Create(string cryptographyManager)
        {
            ICrypto crypto;
            switch (cryptographyManager.ToLower())
            {
                case "simplecryptography":
                    crypto = SimpleCryptographyManager.CryptographyManager;
                    break;
                default:
                    throw new CryptoException("没有具体的加密管理者！");
            }
            return crypto;
        }
    }
}
