﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace UltraNuke.Cryptography
{
    /// <summary>
    /// 提供以 MD5 算法的加密和解密。
    /// </summary>
    public sealed class SimpleMD5CryptoServiceProvider
    {
        private SimpleMD5CryptoServiceProvider()
        {
        }

        /// <summary>
        /// 将明文加密为密码文本
        /// </summary>
        /// <param name="plaintext">明文文本</param>
        public static string Encrypt(string plainText,string key)
        {
            try
            {
                byte[] fromBuffer = Encoding.GetEncoding("UTF-8").GetBytes(string.Concat(plainText,key));
                MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
                byte[] toBuffer = md5Hasher.ComputeHash(fromBuffer);
                return Convert.ToBase64String(toBuffer);
            }
            catch (CryptographicException)
            {
                throw;
            }
        }
    }
}
