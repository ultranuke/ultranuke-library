﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace UltraNuke.Cryptography
{
    /// <summary>
    /// 提供以 RSA 算法的加密和解密。
    /// </summary>
    public sealed class SimpleRSACryptoServiceProvider
    {
        private SimpleRSACryptoServiceProvider()
        {
        }      
        
        /// <summary>
        /// 将明文加密为密码文本
        /// </summary>
        /// <param name="plaintext">明文文本</param>
        /// <param name="publicKey">公钥</param>
        public static string Encrypt(string plainText,string publicKey)
        {
            try
            {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(publicKey);
                byte[] fromBuffer = Encoding.UTF8.GetBytes(plainText);
                byte[] toBuffer = rsa.Encrypt(fromBuffer,false);
                return Convert.ToBase64String(toBuffer);
            }
            catch (CryptographicException)
            {
                throw;
            }


        }

        /// <summary>
        /// 将密码文本解密为明文
        /// </summary>
        /// <param name="cryptoText">密码文本</param>
        /// <param name="privateKey">私钥</param>
        public static string Decrypt(string cypherText, string privateKey)
        {
            try
            {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(privateKey);
                byte[] fromBuffer = Convert.FromBase64String(cypherText);
                byte[] toBuffer = rsa.Decrypt(fromBuffer, false);
                return Encoding.UTF8.GetString(toBuffer);
            }
            catch (CryptographicException)
            {
                throw;
            }
        }
    }
}
