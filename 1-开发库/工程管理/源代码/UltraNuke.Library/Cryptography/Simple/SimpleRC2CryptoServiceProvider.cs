﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace UltraNuke.Cryptography
{
    /// <summary>
    /// 提供以 RC2 算法的加密和解密。
    /// </summary>
    public sealed class SimpleRC2CryptoServiceProvider
    {

        private SimpleRC2CryptoServiceProvider()
        {
        }

        /// <summary>
        /// 将明文加密为密码文本
        /// </summary>
        /// <param name="plaintext">明文文本</param>
        /// <param name="key">密钥</param>
        public static string Encrypt(string plainText,string key)
        {
            byte[] byteKey = Encoding.UTF8.GetBytes(key);
            RC2CryptoServiceProvider rc2 = new RC2CryptoServiceProvider();
            rc2.IV = byteKey;
            rc2.Key = byteKey;

            MemoryStream ms = new MemoryStream();
            CryptoStream encStream = new CryptoStream(ms, rc2.CreateEncryptor(), CryptoStreamMode.Write);
            StreamWriter sw = new StreamWriter(encStream);
            sw.WriteLine(plainText);
            sw.Close();
            encStream.Close();
            byte[] buffer = ms.ToArray();
            ms.Close();
            string cypherText = Convert.ToBase64String(buffer);
            return cypherText;
        }

        /// <summary>
        /// 将密码文本解密为明文
        /// </summary>
        /// <param name="cryptoText">密码文本</param>
        public static string Decrypt(string cypherText, string key)
        {
            byte[] byteKey = Encoding.UTF8.GetBytes(key);
            RC2CryptoServiceProvider rc2 = new RC2CryptoServiceProvider();
            rc2.IV = byteKey;
            rc2.Key = byteKey;

            byte[] buffer = Convert.FromBase64String(cypherText);
            MemoryStream ms = new MemoryStream(buffer);
            CryptoStream encStream = new CryptoStream(ms, rc2.CreateDecryptor(), CryptoStreamMode.Read);
            StreamReader sr = new StreamReader(encStream);
            string val = sr.ReadLine();
            sr.Close();
            encStream.Close();
            ms.Close();
            return val;
        }
    }
}
