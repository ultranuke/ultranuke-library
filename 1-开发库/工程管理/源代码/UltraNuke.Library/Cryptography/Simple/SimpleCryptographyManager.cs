﻿using System;
using UltraNuke.Library.Properties;

namespace UltraNuke.Cryptography
{
    /// <summary>
    /// SimpleCryptographyManager提供以简单方式构建的应用程序加密操作
    /// </summary>
    public class SimpleCryptographyManager : ICrypto
    {
        #region 初始化
        private SimpleCryptographyManager()
        {
        }

        public static SimpleCryptographyManager CryptographyManager
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObject)
                    {
                        if (instance == null)
                        {
                            instance = new SimpleCryptographyManager();
                        }
                    }
                }
                return instance;
            }
        }
        #endregion

        #region ICryptographer 成员
        /// <summary>
        /// 将明文加密为密码文本
        /// </summary>
        /// <param name="plaintext">明文文本</param>
        public string Encrypt(string plainText)
        {
            return Encrypt(plainText, (CryptoMode)Enum.Parse(typeof(CryptoMode), Resources.DefaultCryptographyType));
        }

        /// <summary>
        /// 将明文加密为密码文本
        /// </summary>
        /// <param name="plaintext">明文文本</param>
        /// <param name="cryptoMode">加密方式</param>
        public string Encrypt(string plainText,CryptoMode cryptoMode)
        {
            string cryptoText = string.Empty;
            switch (cryptoMode)
            {
                case CryptoMode.DES:
                    cryptoText = SimpleDESCryptoServiceProvider.Encrypt(plainText, Resources.DefaultDESCryptoKey);
                    break;
                case CryptoMode.RC2:
                    cryptoText = SimpleRC2CryptoServiceProvider.Encrypt(plainText, Resources.DefaultRC2CryptoKey);
                    break;
                case CryptoMode.RSA:
                    cryptoText = SimpleRSACryptoServiceProvider.Encrypt(plainText, Resources.DefaultRSACryptoPublicKey);
                    break;
                case CryptoMode.MD5:
                    cryptoText = SimpleMD5CryptoServiceProvider.Encrypt(plainText, Resources.DefaultMD5CryptoKey);
                    break;
            }
            return cryptoText;
        }

        /// <summary>
        /// 将密码文本解密为明文
        /// </summary>
        /// <param name="cryptoText">密码文本</param>
        public string Decrypt(string cryptoText)
        {
            return Decrypt(cryptoText, (CryptoMode)Enum.Parse(typeof(CryptoMode), Resources.DefaultCryptographyType));
        }

        /// <summary>
        /// 将密码文本解密为明文
        /// </summary>
        /// <param name="cryptoText">密码文本</param>
        /// <param name="cryptoMode">加密方式</param>
        public string Decrypt(string cryptoText, CryptoMode cryptoMode)
        {
            string plainText = string.Empty;
            switch (cryptoMode)
            {
                case CryptoMode.DES:
                    plainText = SimpleDESCryptoServiceProvider.Decrypt(cryptoText, Resources.DefaultDESCryptoKey);
                    break;
                case CryptoMode.RC2:
                    plainText = SimpleRC2CryptoServiceProvider.Decrypt(cryptoText, Resources.DefaultRC2CryptoKey);
                    break;
                case CryptoMode.RSA:
                    plainText = SimpleRSACryptoServiceProvider.Decrypt(cryptoText, Resources.DefaultRSACryptoPrivateKey);
                    break;
            }
            return plainText;
        }
        #endregion

        #region 字段
        private static SimpleCryptographyManager instance;
        private static object lockObject = new object();
        #endregion
    }
}
