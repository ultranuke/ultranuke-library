﻿
namespace UltraNuke.Cryptography
{
    /// <summary>
    /// ICrypto提供应用程序加密操作的接口
    /// </summary>
    public interface ICrypto
    {
        /// <summary>
        /// 将明文加密为密码文本
        /// </summary>
        /// <param name="plaintext">明文文本</param>
        string Encrypt(string plainText);

        /// <summary>
        /// 将明文加密为密码文本
        /// </summary>
        /// <param name="plaintext">明文文本</param>
        /// <param name="cryptoMode">加密方式</param>
        string Encrypt(string plainText, CryptoMode cryptoMode);

        /// <summary>
        /// 将密码文本解密为明文
        /// </summary>
        /// <param name="cryptoText">密码文本</param>
        string Decrypt(string cryptoText);

        /// <summary>
        /// 将密码文本解密为明文
        /// </summary>
        /// <param name="cryptoText">密码文本</param>
        /// <param name="cryptoMode">加密方式</param>
        string Decrypt(string cryptoText, CryptoMode cryptoMode);
    }
}
