﻿using UltraNuke.Library.Properties;

namespace UltraNuke.Validation
{
    /// <summary>
    /// 验证处理工厂类
    /// </summary>
    public sealed class ValidFactory
    {
        private ValidFactory()
        {            
        }
        
        /// <summary>
        /// 创建工厂实例
        /// </summary>
        /// <returns>验证接口</returns>
        public static IValid Create()
        {
            return Create(Resources.DefaultValidManager);
        }

        /// <summary>
        /// 通过名称创建实例
        /// </summary>
        /// <param name="validManager">验证管理者</param>
        /// <returns>验证接口</returns>
        public static IValid Create(string validManager)
        {
            IValid valid;
            switch (validManager.ToLower())
            {
                case "simplevalidation":
                    valid = SimpleValidationManager.ValidationManager;
                    break;
                default:
                    throw new ValidException("没有具体的加密管理者！");
            }
            return valid;
        }
    }
}
