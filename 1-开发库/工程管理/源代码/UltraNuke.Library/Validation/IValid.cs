﻿
namespace UltraNuke.Validation
{
    /// <summary>
    /// IValid提供应用程序验证操作的接口
    /// </summary>
    public interface IValid
    {
        #region 验证字符串是否为空
        /// <summary>
        /// 验证输入字符串是否为空
        /// </summary>
        /// <param name="input">输入字符串</param>      
        void ValidNullOrEmpty(string input);

        /// <summary>
        /// 验证输入字符串是否为空
        /// </summary>
        /// <param name="input">输入字符串</param>  
        /// <param name="message">描述错误的消息</param>    
        void ValidNullOrEmpty(string input, string message);
        #endregion

        #region 验证字符串长度
        /// <summary>
        /// 验证输入字符串是否在指定长度范围内
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="characterType">字符串类型</param>
        /// <param name="upperBound">字符串上限</param>        
        void ValidStringLength(string input, CharacterType characterType, int upperBound);

        /// <summary>
        /// 验证输入字符串是否在指定长度范围内
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="characterType">字符串类型</param>
        /// <param name="upperBound">字符串下限</param>
        /// <param name="upperBound">字符串上限</param>
        void ValidStringLength(string input, CharacterType characterType, int lowerBound, int upperBound);

        /// <summary>
        /// 验证输入字符串是否在指定长度范围内
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="characterType">字符串类型</param>
        /// <param name="upperBound">字符串下限</param>
        /// <param name="upperBound">字符串上限</param>
        /// <param name="message">描述错误的消息</param>
        void ValidStringLength(string input, CharacterType characterType, int lowerBound, int upperBound, string message);
        #endregion

        #region 验证Email格式
        /// <summary>
        /// 验证输入字符串是否符合Email格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        void ValidEmail(string input);

        /// <summary>
        /// 验证输入字符串是否符合Email格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="message">描述错误的消息</param>
        void ValidEmail(string input, string message);
        #endregion

        #region 验证IP格式
        /// <summary>
        /// 验证字符串是否符合IP格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        void ValidIP(string input);

        /// <summary>
        /// 验证字符串是否符合IP格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="message">描述错误的消息</param>
        void ValidIP(string input, string message);
        #endregion

        #region 验证URL格式
        /// <summary>
        /// 验证字符串是否符合URL格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        void ValidUrl(string input);

        /// <summary>
        /// 验证字符串是否符合URL格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="message">描述错误的消息</param>
        void ValidUrl(string input, string message);
        #endregion

        #region 验证电话号码格式
        /// <summary>
        /// 验证字符串是否符合电话号码格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        void ValidTel(string input);

        /// <summary>
        /// 验证字符串是否符合电话号码格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="message">描述错误的消息</param>
        void ValidTel(string input, string message);
        #endregion

        #region 验证字符串是否包含后缀名
        /// <summary>
        /// 验证字符串是否包含后缀名
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="fix">后缀名（如：gif|jpg）</param>
        void ValidPostfix(string input, string fix);

        /// <summary>
        /// 验证字符串是否包含后缀名
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="fix">后缀名（如：gif|jpg）</param>
        void ValidPostfix(string input, string fix, string message);
        #endregion

        #region 验证字符串是否符合正则表达式定义的格式要求
        /// <summary>
        /// 验证字符串是否符合正则表达式定义的格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="regex">正则表达式</param>
        /// <param name="message">描述错误的消息</param>
        void ValidRegex(string input, string regex, string message);
        #endregion
    }
}
