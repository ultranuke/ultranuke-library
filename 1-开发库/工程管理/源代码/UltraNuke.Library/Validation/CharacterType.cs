﻿
namespace UltraNuke.Validation
{
    /// <summary>
    /// 字符串类型
    /// </summary>
    public enum CharacterType
    {
        VarChar = 0,
        NVarChar = 1
    }
}
