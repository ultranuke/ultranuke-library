﻿using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using UltraNuke.Logging;

namespace UltraNuke.Validation
{
    /// <summary>
    /// SimpleValidationManager提供应用程序验证操作
    /// </summary>
    public class SimpleValidationManager : IValid
    {
        private SimpleValidationManager()
        {
        }

        public static SimpleValidationManager ValidationManager
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObject)
                    {
                        if (instance == null)
                        {
                            instance = new SimpleValidationManager();
                        }
                    }
                }
                return instance;
            }
        }

        #region 验证字符串是否为空
        /// <summary>
        /// 验证输入字符串是否为空
        /// </summary>
        /// <param name="input">输入字符串</param>      
        public void ValidNullOrEmpty(string input)
        {
            ValidNullOrEmpty(input, "输入字符串不能为空，请检查！");
        }

        /// <summary>
        /// 验证输入字符串是否为空
        /// </summary>
        /// <param name="input">输入字符串</param>  
        /// <param name="message">描述错误的消息</param>    
        public void ValidNullOrEmpty(string input, string message)
        {
            if (string.IsNullOrEmpty(input))
            {
                ThrowAndLog(message);
            }
        }
        #endregion

        #region 验证字符串长度
        /// <summary>
        /// 验证输入字符串是否在指定长度范围内
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="characterType">字符串类型</param>
        /// <param name="upperBound">字符串上限</param>        
        public void ValidStringLength(string input,CharacterType characterType,int upperBound)
        {
            ValidStringLength(input, characterType,0,upperBound);
        }
        
        /// <summary>
        /// 验证输入字符串是否在指定长度范围内
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="characterType">字符串类型</param>
        /// <param name="upperBound">字符串下限</param>
        /// <param name="upperBound">字符串上限</param>
        public void ValidStringLength(string input, CharacterType characterType, int lowerBound, int upperBound)
        {
            ValidStringLength(input, characterType, lowerBound, upperBound, string.Format("输入字符串不在（{0}-{1}）长度范围内，请检查！", lowerBound, upperBound));
        }

        /// <summary>
        /// 验证输入字符串是否在指定长度范围内
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="characterType">字符串类型</param>
        /// <param name="upperBound">字符串下限</param>
        /// <param name="upperBound">字符串上限</param>
        /// <param name="message">描述错误的消息</param>
        public void ValidStringLength(string input, CharacterType characterType, int lowerBound, int upperBound, string message)
        {
            int ret = 0;
            if (!string.IsNullOrEmpty(input))
            {
                if (characterType == CharacterType.VarChar)
                {
                    Encoding encoding = Encoding.GetEncoding("gb2312");
                    ret = encoding.GetBytes(input).Length;
                }
                else
                {
                    ret = input.Length;
                }
            }
            if (ret < lowerBound || ret > upperBound)
            {
                ThrowAndLog(message);
            }
        }
        #endregion

        #region 验证Email格式
        /// <summary>
        /// 验证输入字符串是否符合Email格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        public void ValidEmail(string input)
        {
            ValidEmail(input, "输入字符串不符合Email格式要求，请检查！");
        }
        
        /// <summary>
        /// 验证输入字符串是否符合EMAIL格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="message">描述错误的消息</param>
        public void ValidEmail(string input,string message)
        {
            string regex = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            ValidRegex(input, regex, message);
        }
        #endregion

        #region 验证IP格式
        /// <summary>
        /// 验证字符串是否符合IP格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        public void ValidIP(string input)
        {
            ValidIP(input, "输入字符串不符合IP格式要求，请检查！");
        }

        /// <summary>
        /// 验证字符串是否符合IP格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="message">描述错误的消息</param>
        public void ValidIP(string input, string message)
        {
            string regex = @"^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$";
            ValidRegex(input, regex, message);
        }
        #endregion

        #region 验证URL格式
        /// <summary>
        /// 验证字符串是否符合URL格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        public void ValidUrl(string input)
        {
            ValidUrl(input, "输入字符串不符合URL格式要求，请检查！");
        }

        /// <summary>
        /// 验证字符串是否符合URL格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="message">描述错误的消息</param>
        public void ValidUrl(string input, string message)
        {
            string regex = @"^[a-zA-z]+://(\w+(-\w+)*)(\.(\w+(-\w+)*))*(\?\S*)?$";
            ValidRegex(input, regex, message);
        }
        #endregion

        #region 验证电话号码格式
        /// <summary>
        /// 验证字符串是否符合电话号码格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        public void ValidTel(string input)
        {
            ValidTel(input, "输入字符串不符合电话号码格式要求，请检查！");
        }

        /// <summary>
        /// 验证字符串是否符合电话号码格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="message">描述错误的消息</param>
        public void ValidTel(string input, string message)
        {
            string regex = @"(\d+-)?(\d{4}-?\d{7}|\d{3}-?\d{8}|^\d{7,8})(-\d+)?";
            ValidRegex(input, regex, message);
        }
        #endregion

        #region 验证字符串是否包含后缀名
        /// <summary>
        /// 验证字符串是否包含后缀名
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="fix">后缀名（如：gif|jpg）</param>
        public void ValidPostfix(string input, string fix)
        {
            ValidPostfix(input, fix, string.Format("输入字符串不包含后缀名({0})，请检查！",fix));
        }

        /// <summary>
        /// 验证字符串是否包含后缀名
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="fix">后缀名（如：gif|jpg）</param>
        public void ValidPostfix(string input, string fix, string message)
        {
            string regex = string.Format(CultureInfo.InvariantCulture, @"\.(?i:{0})$", fix);
            ValidRegex(input, regex, message);
        }
        #endregion

        #region 验证字符串是否符合正则表达式定义的格式要求
        /// <summary>
        /// 验证输入字符串是否符合正则表达式定义的格式要求
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="regex">正则表达式</param>
        /// <param name="message">描述错误的消息</param>
        public void ValidRegex(string input, string regex, string message)
        {
            if (!Regex.IsMatch(input, regex))
            {
                ThrowAndLog(message);
            }
        }
        #endregion

        #region 字段
        private readonly static ILog Log = LogFactory.Create(typeof(SimpleValidationManager));
        private static SimpleValidationManager instance;
        private static object lockObject = new object();
        #endregion

        #region 方法
        private static void ThrowAndLog(string message)
        {
            ValidException e = new ValidException(message);
            Log.Write(e.Message, e, LogLevel.Error);
            throw e;
        }
        #endregion
    }
}
