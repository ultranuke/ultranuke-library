﻿using System;
using UltraNuke.Validation;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 年连续编号回收信息
    /// </summary>
    public class YearNumberCanUsedInfo : BaseNumberCanUsedInfo
    {
        private YearNumberInfo yearNumber;
        /// <summary>
        /// 编号信息
        /// </summary>
        public virtual YearNumberInfo YearNumber
        {
            set
            {
                yearNumber = value;
            }
            get { return yearNumber; }
        }

        private int year;
        /// <summary>
        /// 年
        /// </summary>
        public virtual int Year
        {
            set
            {
                year = value;
            }
            get { return year; }
        }
    }
}
