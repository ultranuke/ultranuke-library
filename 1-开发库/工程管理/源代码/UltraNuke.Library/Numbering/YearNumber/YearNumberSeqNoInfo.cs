﻿using System;
using UltraNuke.Validation;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 年下一顺序号信息
    /// </summary>
    public class YearNumberSeqNoInfo
    {
        private Guid id;
        /// <summary>
        /// 年下一顺序号ID
        /// </summary>
        public virtual Guid Id
        {
            set
            {
                id = value;
            }
            get { return id; }
        }

        private YearNumberInfo yearNumber;
        /// <summary>
        /// 编号信息
        /// </summary>
        public virtual YearNumberInfo YearNumber
        {
            set
            {
                yearNumber = value;
            }
            get { return yearNumber; }
        }

        private int year;
        /// <summary>
        /// 年
        /// </summary>
        public virtual int Year
        {
            set
            {
                year = value;
            }
            get { return year; }
        }

        private int nextSeqNo;
        /// <summary>
        /// 下一顺序号
        /// </summary>
        public virtual int NextSeqNo
        {
            set
            {
                nextSeqNo = value;
            }
            get { return nextSeqNo; }
        }

        #region 字段
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion
    }
}
