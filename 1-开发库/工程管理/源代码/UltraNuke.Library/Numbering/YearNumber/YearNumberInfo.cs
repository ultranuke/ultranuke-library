﻿using System;
using System.Collections.Generic;
using UltraNuke.Validation;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 年连续编号信息
    /// </summary>
    public class YearNumberInfo : BaseNumberInfo
    {
        private IList<YearNumberSeqNoInfo> yearNumberSeqNos;
        /// <summary>
        /// 年下一顺序号集合
        /// </summary>
        public virtual IList<YearNumberSeqNoInfo> YearNumberSeqNos
        {
            set { yearNumberSeqNos = value; }
            get
            {
                if (yearNumberSeqNos == null)
                    yearNumberSeqNos = new List<YearNumberSeqNoInfo>();

                return yearNumberSeqNos;
            }
        }

        private IList<YearNumberCanUsedInfo> yearNumberCanUseds;
        /// <summary>
        /// 编号回收集合
        /// </summary>
        public virtual IList<YearNumberCanUsedInfo> YearNumberCanUseds
        {
            set { yearNumberCanUseds = value; }
            get
            {
                if (yearNumberCanUseds == null)
                    yearNumberCanUseds = new List<YearNumberCanUsedInfo>();

                return yearNumberCanUseds;
            }
        }

        #region 字段
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion
    }
}


