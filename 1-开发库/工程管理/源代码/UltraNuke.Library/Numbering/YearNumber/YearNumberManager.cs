﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UltraNuke.Data;

namespace UltraNuke.Numbering
{
	/// <summary>
    /// 提供应用程序年连续编号操作
    /// </summary>
    public class YearNumberManager : INumber
    {
        /// <summary>
        /// 生成连续编号
        /// </summary>
        /// <param name="name">编号名称</param>
        /// <param name="numberDate">业务日期</param>
        /// <returns>业务编号</returns>
        public string GenerateNumber(string name, DateTime numberDate)
        {
            ISession session = NHibernateUtils.Session;
            ITransaction trans = null;
            try
            {

                IList<YearNumberInfo> list = session.CreateCriteria(typeof(YearNumberInfo))
                    .Add(Restrictions.Like("Name", name))
                    .List<YearNumberInfo>();

                if (list.Count == 0)
                {
                    throw new NumberException("未找到编号配置，请检查！");
                }

                YearNumberInfo numberInfo = list[0];

                int seqNo = 1;
                int year = numberDate.Year;
                string returnNumber;

                YearNumberCanUsedInfo canUsedInfo = numberInfo
                    .YearNumberCanUseds
                    .FirstOrDefault<YearNumberCanUsedInfo>(
                    info => info.Year == year);

                if (canUsedInfo != null)
                {
                    returnNumber = canUsedInfo.RegNo;
                    numberInfo.YearNumberCanUseds.Remove(canUsedInfo);
                }
                else
                {
                    YearNumberSeqNoInfo seqNoInfo = numberInfo
                        .YearNumberSeqNos
                        .FirstOrDefault<YearNumberSeqNoInfo>(
                        info => info.Year == year);

                    if (seqNoInfo != null)
                    {
                        seqNo = seqNoInfo.NextSeqNo;
                        seqNoInfo.NextSeqNo = seqNoInfo.NextSeqNo + 1;
                    }
                    else
                    {
                        YearNumberSeqNoInfo newSeqNo = new YearNumberSeqNoInfo();
                        newSeqNo.YearNumber = numberInfo;
                        newSeqNo.Year = year;
                        newSeqNo.NextSeqNo = seqNo + 1;
                        numberInfo.YearNumberSeqNos.Add(newSeqNo);
                    }

                    returnNumber = numberInfo.NumberFormat
                    .Replace("nnnn", seqNo.ToString().PadLeft(numberInfo.SeqLength, '0'))
                    .Replace("yyyy", numberDate.ToString("yyyy"))
                    .Replace("MM", numberDate.ToString("MM"))
                    .Replace("dd", numberDate.ToString("dd"));
                }

                trans = session.BeginTransaction();
                session.Update(numberInfo);
                trans.Commit();

                return returnNumber;
            }
            catch
            {
                if (trans != null) trans.Rollback();
                throw;
            }
            finally
            {
                NHibernateUtils.CloseSession();
            }
        }

        
        /// <summary>
        /// 回收年连续编号
        /// </summary>
        /// <param name="name">编号名称</param>
        /// <param name="numberDate">业务日期</param>
        /// <param name="regNo">业务编号</param>
        public void RecycleNumber(string name, DateTime numberDate, string regNo)
        {
            ISession session = NHibernateUtils.Session;
            try
            {
                IList<YearNumberInfo> list = session.CreateCriteria(typeof(YearNumberInfo))
                        .Add(Restrictions.Like("Name", name))
                        .List<YearNumberInfo>();

                if (list.Count == 0)
                {
                    throw new NumberException("未找到编号配置，请检查！");
                }

                YearNumberInfo numberInfo = list[0];

                YearNumberCanUsedInfo canUsedInfo = new YearNumberCanUsedInfo();
                canUsedInfo.YearNumber = numberInfo;
                canUsedInfo.Year = numberDate.Year;
                canUsedInfo.RegNo = regNo;
                numberInfo.YearNumberCanUseds.Add(canUsedInfo);

                session.Update(numberInfo);
                session.Flush();

            }
            catch
            {
                throw;
            }
            finally
            {
                NHibernateUtils.CloseSession();
            }
        }
    }
}


