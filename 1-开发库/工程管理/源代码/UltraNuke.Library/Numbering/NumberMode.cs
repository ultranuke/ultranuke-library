﻿
namespace UltraNuke.Numbering
{
    /// <summary>
    /// 编号模式
    /// </summary>
    public enum NumberMode
    {
        连续编号,
        按年连续编号,
        按月连续编号,
        按日连续编号
    }
}
