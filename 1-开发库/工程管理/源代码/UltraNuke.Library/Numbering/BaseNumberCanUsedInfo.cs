﻿using System;
using UltraNuke.Validation;

namespace UltraNuke.Numbering
{
    public class BaseNumberCanUsedInfo
    {
        #region fields
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion

        #region properties
        private Guid id;
        /// <summary>
        /// 编号回收ID
        /// </summary>
        public virtual Guid Id
        {
            set
            {
                id = value;
            }
            get { return id; }
        }

        private string regNo;
        /// <summary>
        /// 业务编号
        /// </summary>
        public virtual string RegNo
        {
            set
            {
                Valid.ValidNullOrEmpty(value, "业务编号不能为空！");
                Valid.ValidStringLength(value, CharacterType.VarChar, 0, 50, "业务编号不能大于50！");
                regNo = value;
            }
            get { return regNo; }
        }
        #endregion
    }
}
