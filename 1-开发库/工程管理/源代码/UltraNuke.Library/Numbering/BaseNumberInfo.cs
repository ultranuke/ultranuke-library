﻿using System;
using UltraNuke.Validation;

namespace UltraNuke.Numbering
{
    public class BaseNumberInfo
    {
        private Guid id;
        /// <summary>
        /// 编号ID
        /// </summary>
        public virtual Guid Id
        {
            set
            {
                id = value;
            }
            get { return id; }
        }

        private string name;
        /// <summary>
        /// 编号名称
        /// </summary>
        public virtual string Name
        {
            set
            {
                Valid.ValidNullOrEmpty(value, "编号名称不能为空！");
                Valid.ValidStringLength(value, CharacterType.NVarChar, 0, 20, "编号名称不能大于20！");

                name = value;
            }
            get { return name; }
        }

        private int seqLength;
        /// <summary>
        /// 顺序号长度
        /// </summary>
        public virtual int SeqLength
        {
            set
            {
                seqLength = value;
            }
            get { return seqLength; }
        }

        private string numberFormat;
        /// <summary>
        /// 编号构造
        /// </summary>
        public virtual string NumberFormat
        {
            set
            {
                Valid.ValidNullOrEmpty(value, "编号构造不能为空！");
                Valid.ValidStringLength(value, CharacterType.NVarChar, 0, 50, "编号构造不能大于50！");

                numberFormat = value;
            }
            get { return numberFormat; }
        }

        private int sortNo;
        /// <summary>
        /// 排序
        /// </summary>
        public virtual int SortNo
        {
            set
            {
                sortNo = value;
            }
            get { return sortNo; }
        }

        #region 字段
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion
    }
}
