﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UltraNuke.Data;
using System;

namespace UltraNuke.Numbering
{
	/// <summary>
    /// 提供应用程序连续编号操作
    /// </summary>
    public class NumberManager : INumber
    {
        /// <summary>
        /// 生成连续编号
        /// </summary>
        /// <param name="name">编号名称</param>
        /// <param name="numberDate">业务日期</param>
        /// <returns>业务编号</returns>
        public string GenerateNumber(string name, DateTime numberDate)
        {
            ISession session = NHibernateUtils.Session;
            ITransaction trans = null;
            try
            {

                IList<NumberInfo> list = session.CreateCriteria(typeof(NumberInfo))
                    .Add(Restrictions.Like("Name", name))
                    .List<NumberInfo>();

                if (list.Count == 0)
                {
                    throw new NumberException("未找到编号配置，请检查！");
                }

                NumberInfo numberInfo = list[0];

                int seqNo = 1;
                string returnNumber;

                NumberCanUsedInfo canUsedInfo = numberInfo
                    .NumberCanUseds
                    .FirstOrDefault<NumberCanUsedInfo>();

                if (canUsedInfo != null)
                {
                    returnNumber = canUsedInfo.RegNo;
                    numberInfo.NumberCanUseds.Remove(canUsedInfo);
                }
                else
                {
                    seqNo = numberInfo.NextSeqNo;
                    numberInfo.NextSeqNo += 1;
                    returnNumber = numberInfo.NumberFormat
                    .Replace("nnnn", seqNo.ToString().PadLeft(numberInfo.SeqLength, '0'))
                    .Replace("yyyy", numberDate.ToString("yyyy"))
                    .Replace("MM", numberDate.ToString("MM"))
                    .Replace("dd", numberDate.ToString("dd"));
                }

                trans = session.BeginTransaction();
                session.Update(numberInfo);
                trans.Commit();

                return returnNumber;

            }
            catch
            {
                if (trans != null) trans.Rollback();
                throw;
            }
            finally
            {
                NHibernateUtils.CloseSession();
            }

        }

        /// <summary>
        /// 回收年连续编号
        /// </summary>
        /// <param name="name">编号名称</param>
        /// <param name="numberDate">业务日期</param>
        /// <param name="regNo">业务编号</param>
        public void RecycleNumber(string name, DateTime numberDate, string regNo)
        {
            ISession session = NHibernateUtils.Session;
            try
            {
                IList<NumberInfo> list = session.CreateCriteria(typeof(NumberInfo))
                        .Add(Restrictions.Like("Name", name))
                        .List<NumberInfo>();

                if (list.Count == 0)
                {
                    throw new NumberException("未找到编号配置，请检查！");
                }

                NumberInfo NumberInfo = list[0];

                NumberCanUsedInfo NumberCanUsedInfo = new NumberCanUsedInfo();
                NumberCanUsedInfo.RegNo = regNo;
                NumberCanUsedInfo.Number = NumberInfo;
                NumberInfo.NumberCanUseds.Add(NumberCanUsedInfo);

                session.Update(NumberInfo);
                session.Flush();

            }
            catch
            {
                throw;
            }
            finally
            {
                NHibernateUtils.CloseSession();
            }
        }
    }
}


