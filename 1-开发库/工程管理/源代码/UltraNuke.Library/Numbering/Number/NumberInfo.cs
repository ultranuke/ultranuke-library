﻿using System;
using System.Collections.Generic;
using UltraNuke.Validation;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 连续编号信息
    /// </summary>
    public class NumberInfo : BaseNumberInfo
    {
        private int nextSeqNo;
        /// <summary>
        /// 下一顺序号
        /// </summary>
        public virtual int NextSeqNo
        {
            set
            {
                nextSeqNo = value;
            }
            get { return nextSeqNo; }
        }

        private IList<NumberCanUsedInfo> numberCanUseds;
        /// <summary>
        /// 编号回收集合
        /// </summary>
        public virtual IList<NumberCanUsedInfo> NumberCanUseds
        {
            set { numberCanUseds = value; }
            get
            {
                if (numberCanUseds == null)
                    numberCanUseds = new List<NumberCanUsedInfo>();

                return numberCanUseds;
            }
        }
    }
}


