﻿using System;
using UltraNuke.Validation;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 连续编号回收信息
    /// </summary>
    public class NumberCanUsedInfo : BaseNumberCanUsedInfo
    {
        private NumberInfo number;
        /// <summary>
        /// 编号信息
        /// </summary>
        public virtual NumberInfo Number
        {
            set
            {
                number = value;
            }
            get { return number; }
        }
    }
}
