﻿using UltraNuke.Library.Properties;
using System;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 编号处理工厂类
    /// </summary>
    public sealed class NumberFactory
    {
        private NumberFactory()
        {            
        }
        
        /// <summary>
        /// 创建工厂实例
        /// </summary>
        /// <returns>编号接口</returns>
        public static INumber Create()
        {
            return Create((NumberMode)Enum.Parse(typeof(NumberMode),Resources.DefaultNumberMode));
        }

        /// <summary>
        /// 创建工厂实例
        /// </summary>
        /// <param name="numberMode">编号模式</param>
        /// <returns>编号接口</returns>
        public static INumber Create(NumberMode numberMode)
        {
            INumber number;
            switch (numberMode)
            {
                case NumberMode.连续编号:
                    number = new NumberManager();
                    break;
                case NumberMode.按年连续编号:
                    number = new YearNumberManager();
                    break;
                case NumberMode.按月连续编号:
                    number = new MonthNumberManager();
                    break;
                case NumberMode.按日连续编号:
                    number = new DayNumberManager();
                    break;
                default:
                    throw new NumberException("没有具体的编号模式！");
            }
            return number;
        }
    }
}
