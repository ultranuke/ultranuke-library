﻿using System;
using System.Collections.Generic;
using UltraNuke.Validation;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 日连续编号信息
    /// </summary>
    public class DayNumberInfo : BaseNumberInfo
    {
        private IList<DayNumberSeqNoInfo> dayNumberSeqNos;
        /// <summary>
        /// 日下一顺序号集合
        /// </summary>
        public virtual IList<DayNumberSeqNoInfo> DayNumberSeqNos
        {
            set { dayNumberSeqNos = value; }
            get
            {
                if (dayNumberSeqNos == null)
                    dayNumberSeqNos = new List<DayNumberSeqNoInfo>();

                return dayNumberSeqNos;
            }
        }

        private IList<DayNumberCanUsedInfo> dayNumberCanUseds;
        /// <summary>
        /// 编号回收集合
        /// </summary>
        public virtual IList<DayNumberCanUsedInfo> DayNumberCanUseds
        {
            set { dayNumberCanUseds = value; }
            get
            {
                if (dayNumberCanUseds == null)
                    dayNumberCanUseds = new List<DayNumberCanUsedInfo>();

                return dayNumberCanUseds;
            }
        }

        #region 字段
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion
    }
}


