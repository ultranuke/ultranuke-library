﻿using System;
using UltraNuke.Validation;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 日连续编号回收信息
    /// </summary>
    public class DayNumberCanUsedInfo : BaseNumberCanUsedInfo
    {
        private DayNumberInfo dayNumber;
        /// <summary>
        /// 编号信息
        /// </summary>
        public virtual DayNumberInfo DayNumber
        {
            set
            {
                dayNumber = value;
            }
            get { return dayNumber; }
        }

        private int year;
        /// <summary>
        /// 年
        /// </summary>
        public virtual int Year
        {
            set
            {
                year = value;
            }
            get { return year; }
        }

        private int month;
        /// <summary>
        /// 月
        /// </summary>
        public virtual int Month
        {
            set
            {
                month = value;
            }
            get { return month; }
        }

        private int day;
        /// <summary>
        /// 天
        /// </summary>
        public virtual int Day
        {
            set
            {
                day = value;
            }
            get { return day; }
        }
    }
}
