﻿using System;
using UltraNuke.Validation;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 日下一顺序号信息
    /// </summary>
    public class DayNumberSeqNoInfo
    {
        private Guid id;
        /// <summary>
        /// 日下一顺序号ID
        /// </summary>
        public virtual Guid Id
        {
            set
            {
                id = value;
            }
            get { return id; }
        }

        private DayNumberInfo dayNumber;
        /// <summary>
        /// 编号信息
        /// </summary>
        public virtual DayNumberInfo DayNumber
        {
            set
            {
                dayNumber = value;
            }
            get { return dayNumber; }
        }

        private int year;
        /// <summary>
        /// 年
        /// </summary>
        public virtual int Year
        {
            set
            {
                year = value;
            }
            get { return year; }
        }

        private int month;
        /// <summary>
        /// 月
        /// </summary>
        public virtual int Month
        {
            set
            {
                month = value;
            }
            get { return month; }
        }

        private int day;
        /// <summary>
        /// 天
        /// </summary>
        public virtual int Day
        {
            set
            {
                day = value;
            }
            get { return day; }
        }

        private int nextSeqNo;
        /// <summary>
        /// 下一顺序号
        /// </summary>
        public virtual int NextSeqNo
        {
            set
            {
                nextSeqNo = value;
            }
            get { return nextSeqNo; }
        }

        #region 字段
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion
    }
}
