﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UltraNuke.Data;

namespace UltraNuke.Numbering
{
	/// <summary>
    /// 提供应用程序日连续编号操作
    /// </summary>
    public class DayNumberManager : INumber
    {
        /// <summary>
        /// 生成连续编号
        /// </summary>
        /// <param name="name">编号名称</param>
        /// <param name="numberDate">业务日期</param>
        /// <returns>业务编号</returns>
        public string GenerateNumber(string name, DateTime numberDate)
        {
            ISession session = NHibernateUtils.Session;
            ITransaction trans = null;
            try
            {

                IList<DayNumberInfo> list = session.CreateCriteria(typeof(DayNumberInfo))
                    .Add(Restrictions.Like("Name", name))
                    .List<DayNumberInfo>();

                if (list.Count == 0)
                {
                    throw new NumberException("未找到编号配置，请检查！");
                }

                DayNumberInfo numberInfo = list[0];

                int seqNo = 1;
                int year = numberDate.Year;
                int month = numberDate.Month;
                int day = numberDate.Day;
                string returnNumber;

                DayNumberCanUsedInfo canUsedInfo = numberInfo
                    .DayNumberCanUseds
                    .FirstOrDefault<DayNumberCanUsedInfo>(
                    info => info.Year == year && info.Month == month && info.Day == day);

                if (canUsedInfo != null)
                {
                    returnNumber = canUsedInfo.RegNo;
                    numberInfo.DayNumberCanUseds.Remove(canUsedInfo);
                }
                else
                {
                    DayNumberSeqNoInfo seqNoInfo = numberInfo
                        .DayNumberSeqNos
                        .FirstOrDefault<DayNumberSeqNoInfo>(
                        info => info.Year == year && info.Month == month && info.Day == day);

                    if (seqNoInfo != null)
                    {
                        seqNo = seqNoInfo.NextSeqNo;
                        seqNoInfo.NextSeqNo = seqNoInfo.NextSeqNo + 1;
                    }
                    else
                    {
                        DayNumberSeqNoInfo newSeqNo = new DayNumberSeqNoInfo();
                        newSeqNo.DayNumber = numberInfo;
                        newSeqNo.Year = year;
                        newSeqNo.Month = month;
                        newSeqNo.Day = day;
                        newSeqNo.NextSeqNo = seqNo + 1;
                        numberInfo.DayNumberSeqNos.Add(newSeqNo);
                    }

                    returnNumber = numberInfo.NumberFormat
                    .Replace("nnnn", seqNo.ToString().PadLeft(numberInfo.SeqLength, '0'))
                    .Replace("yyyy", numberDate.ToString("yyyy"))
                    .Replace("MM", numberDate.ToString("MM"))
                    .Replace("dd", numberDate.ToString("dd"));
                }

                trans = session.BeginTransaction();
                session.Update(numberInfo);
                trans.Commit();

                return returnNumber;
            }
            catch
            {
                if (trans != null) trans.Rollback();
                throw;
            }
            finally
            {
                NHibernateUtils.CloseSession();
            }
        }

        
        /// <summary>
        /// 回收日连续编号
        /// </summary>
        /// <param name="name">编号名称</param>
        /// <param name="numberDate">业务日期</param>
        /// <param name="regNo">业务编号</param>
        public void RecycleNumber(string name, DateTime numberDate, string regNo)
        {
            ISession session = NHibernateUtils.Session;
            try
            {
                IList<DayNumberInfo> list = session.CreateCriteria(typeof(DayNumberInfo))
                        .Add(Restrictions.Like("Name", name))
                        .List<DayNumberInfo>();

                if (list.Count == 0)
                {
                    throw new NumberException("未找到编号配置，请检查！");
                }

                DayNumberInfo numberInfo = list[0];

                DayNumberCanUsedInfo canUsedInfo = new DayNumberCanUsedInfo();
                canUsedInfo.DayNumber = numberInfo;
                canUsedInfo.Year = numberDate.Year;
                canUsedInfo.Month = numberDate.Month;
                canUsedInfo.Day = numberDate.Day;
                canUsedInfo.RegNo = regNo;
                numberInfo.DayNumberCanUseds.Add(canUsedInfo);

                session.Update(numberInfo);
                session.Flush();

            }
            catch
            {
                throw;
            }
            finally
            {
                NHibernateUtils.CloseSession();
            }
        }
    }
}


