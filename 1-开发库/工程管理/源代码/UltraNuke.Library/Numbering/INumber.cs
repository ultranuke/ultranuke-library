﻿using System;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// INumber提供应用程序生成及回收编号操作的接口
    /// </summary>
    public interface INumber
    {
        /// <summary>
        /// 生成编号
        /// </summary>
        /// <param name="name">编号名称</param>
        /// <param name="numberDate">业务日期</param>
        /// <returns>业务编号</returns>
        string GenerateNumber(string name, DateTime numberDate);

        /// <summary>
        /// 回收编号
        /// </summary>
        /// <param name="name">编号名称</param>
        /// <param name="numberDate">业务日期</param>
        /// <param name="regNo">业务编号</param>
        void RecycleNumber(string name, DateTime numberDate, string regNo);
    }
}
