﻿using System;
using UltraNuke.Validation;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 月连续编号回收信息
    /// </summary>
    public class MonthNumberCanUsedInfo : BaseNumberCanUsedInfo
    {
        private MonthNumberInfo monthNumber;
        /// <summary>
        /// 编号信息
        /// </summary>
        public virtual MonthNumberInfo MonthNumber
        {
            set
            {
                monthNumber = value;
            }
            get { return monthNumber; }
        }

        private int year;
        /// <summary>
        /// 年
        /// </summary>
        public virtual int Year
        {
            set
            {
                year = value;
            }
            get { return year; }
        }

        private int month;
        /// <summary>
        /// 月
        /// </summary>
        public virtual int Month
        {
            set
            {
                month = value;
            }
            get { return month; }
        }
    }
}
