﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UltraNuke.Data;

namespace UltraNuke.Numbering
{
	/// <summary>
    /// 提供应用程序月连续编号操作
    /// </summary>
    public class MonthNumberManager : INumber
    {
        #region methods
        /// <summary>
        /// 生成连续编号
        /// </summary>
        /// <param name="name">编号名称</param>
        /// <param name="numberDate">业务日期</param>
        /// <returns>业务编号</returns>
        public string GenerateNumber(string name, DateTime numberDate)
        {
            ISession session = NHibernateUtils.Session;
            ITransaction trans = null;
            try
            {

                IList<MonthNumberInfo> list = session.CreateCriteria(typeof(MonthNumberInfo))
                    .Add(Restrictions.Like("Name", name))
                    .List<MonthNumberInfo>();

                if (list.Count == 0)
                {
                    throw new NumberException("未找到编号配置，请检查！");
                }

                MonthNumberInfo numberInfo = list[0];

                int seqNo = 1;
                int year = numberDate.Year;
                int month = numberDate.Month;
                string returnNumber;

                MonthNumberCanUsedInfo canUsedInfo = numberInfo
                    .MonthNumberCanUseds
                    .FirstOrDefault<MonthNumberCanUsedInfo>(
                    info => info.Year == year && info.Month == month);

                if (canUsedInfo != null)
                {
                    returnNumber = canUsedInfo.RegNo;
                    numberInfo.MonthNumberCanUseds.Remove(canUsedInfo);
                }
                else
                {
                    MonthNumberSeqNoInfo seqNoInfo = numberInfo
                        .MonthNumberSeqNos
                        .FirstOrDefault<MonthNumberSeqNoInfo>(
                        info => info.Year == year && info.Month == month);

                    if (seqNoInfo != null)
                    {
                        seqNo = seqNoInfo.NextSeqNo;
                        seqNoInfo.NextSeqNo = seqNoInfo.NextSeqNo + 1;
                    }
                    else
                    {
                        MonthNumberSeqNoInfo newSeqNo = new MonthNumberSeqNoInfo();
                        newSeqNo.MonthNumber = numberInfo;
                        newSeqNo.Year = year;
                        newSeqNo.Month = month;
                        newSeqNo.NextSeqNo = seqNo + 1;
                        numberInfo.MonthNumberSeqNos.Add(newSeqNo);
                    }

                    returnNumber = numberInfo.NumberFormat
                    .Replace("nnnn", seqNo.ToString().PadLeft(numberInfo.SeqLength, '0'))
                    .Replace("yyyy", numberDate.ToString("yyyy"))
                    .Replace("MM", numberDate.ToString("MM"))
                    .Replace("dd", numberDate.ToString("dd"));
                }

                trans = session.BeginTransaction();
                session.Update(numberInfo);
                trans.Commit();

                return returnNumber;
            }
            catch
            {
                if (trans != null) trans.Rollback();
                throw;
            }
            finally
            {
                NHibernateUtils.CloseSession();
            }
        }

        /// <summary>
        /// 回收月连续编号
        /// </summary>
        /// <param name="name">编号名称</param>
        /// <param name="numberDate">业务日期</param>
        /// <param name="regNo">业务编号</param>
        public void RecycleNumber(string name, DateTime numberDate, string regNo)
        {
            ISession session = NHibernateUtils.Session;
            try
            {
                IList<MonthNumberInfo> list = session.CreateCriteria(typeof(MonthNumberInfo))
                        .Add(Restrictions.Like("Name", name))
                        .List<MonthNumberInfo>();

                if (list.Count == 0)
                {
                    throw new NumberException("未找到编号配置，请检查！");
                }

                MonthNumberInfo numberInfo = list[0];

                MonthNumberCanUsedInfo canUsedInfo = new MonthNumberCanUsedInfo();
                canUsedInfo.MonthNumber = numberInfo;
                canUsedInfo.Year = numberDate.Year;
                canUsedInfo.Month = numberDate.Month;
                canUsedInfo.RegNo = regNo;
                numberInfo.MonthNumberCanUseds.Add(canUsedInfo);

                session.Update(numberInfo);
                session.Flush();

            }
            catch
            {
                throw;
            }
            finally
            {
                NHibernateUtils.CloseSession();
            }
        }
        #endregion
    }
}


