﻿using System;
using UltraNuke.Validation;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 月下一顺序号信息
    /// </summary>
    public class MonthNumberSeqNoInfo
    {
        private Guid id;
        /// <summary>
        /// 月下一顺序号ID
        /// </summary>
        public virtual Guid Id
        {
            set
            {
                id = value;
            }
            get { return id; }
        }

        private MonthNumberInfo monthNumber;
        /// <summary>
        /// 编号信息
        /// </summary>
        public virtual MonthNumberInfo MonthNumber
        {
            set
            {
                monthNumber = value;
            }
            get { return monthNumber; }
        }

        private int year;
        /// <summary>
        /// 年
        /// </summary>
        public virtual int Year
        {
            set
            {
                year = value;
            }
            get { return year; }
        }

        private int month;
        /// <summary>
        /// 月
        /// </summary>
        public virtual int Month
        {
            set
            {
                month = value;
            }
            get { return month; }
        }

        private int nextSeqNo;
        /// <summary>
        /// 下一顺序号
        /// </summary>
        public virtual int NextSeqNo
        {
            set
            {
                nextSeqNo = value;
            }
            get { return nextSeqNo; }
        }

        #region 字段
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion
    }
}
