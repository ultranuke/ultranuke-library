﻿using System;
using System.Collections.Generic;
using UltraNuke.Validation;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 月连续编号信息
    /// </summary>
    public class MonthNumberInfo : BaseNumberInfo
    {
        #region fields
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion

        #region properties
        private IList<MonthNumberSeqNoInfo> monthNumberSeqNos;
        /// <summary>
        /// 月下一顺序号集合
        /// </summary>
        public virtual IList<MonthNumberSeqNoInfo> MonthNumberSeqNos
        {
            set { monthNumberSeqNos = value; }
            get
            {
                if (monthNumberSeqNos == null)
                    monthNumberSeqNos = new List<MonthNumberSeqNoInfo>();

                return monthNumberSeqNos;
            }
        }

        private IList<MonthNumberCanUsedInfo> monthNumberCanUseds;
        /// <summary>
        /// 编号回收集合
        /// </summary>
        public virtual IList<MonthNumberCanUsedInfo> MonthNumberCanUseds
        {
            set { monthNumberCanUseds = value; }
            get
            {
                if (monthNumberCanUseds == null)
                    monthNumberCanUseds = new List<MonthNumberCanUsedInfo>();

                return monthNumberCanUseds;
            }
        }
        #endregion
    }
}


