﻿using System;
using UltraNuke.Data;

namespace UltraNuke.Security
{
    /// <summary>
    /// 提供用于功能类别的管理功能接口
    /// </summary>
    public interface IFunctionCategoryManager : IData<FunctionCategoryInfo>
    {
    }
}


