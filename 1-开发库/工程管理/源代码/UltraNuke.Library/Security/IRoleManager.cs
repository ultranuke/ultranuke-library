﻿using System;
using System.Collections.Generic;
using UltraNuke.Data;

namespace UltraNuke.Security
{
    /// <summary>
    /// 提供用于角色的管理功能接口
    /// </summary>
    public interface IRoleManager : IData<RoleInfo>
    {
        /// <summary>
        /// 获取指定机构的所有角色集合。
        /// </summary>
        /// <param name="store">指定的店铺</param>
        /// <returns>角色的集合</returns>
        IList<RoleInfo> FindRoles(OrganizationInfo store);

        /// <summary>
        /// 将指定的用户添加到指定的角色中。
        /// </summary>
        /// <param name="userInfo">用户信息</param>
        /// <param name="roleInfo">角色信息</param>
        void AddUserToRole(UserInfo userInfo,RoleInfo roleInfo);


        /// <summary>
        /// 从指定的角色中移除指定的用户。
        /// </summary>
        /// <param name="userInfo">用户信息</param>
        /// <param name="roleInfo">角色信息</param>
        void RemoveUserFromRole(UserInfo userInfo,RoleInfo roleInfo);

        /// <summary>
        /// 获取一个用户所属角色的列表
        /// </summary>
        /// <param name="userInfo">用户信息</param>
        /// <returns>角色的集合</returns>
        IList<RoleInfo> GetRolesForUser(UserInfo userInfo);

        /// <summary>
        /// 获取一个角色所有用户的列表
        /// </summary>
        /// <param name="roleInfo">角色信息</param>
        /// <returns>用户的集合</returns>
        IList<UserInfo> FindUsersInRole(RoleInfo roleInfo);
    }
}


