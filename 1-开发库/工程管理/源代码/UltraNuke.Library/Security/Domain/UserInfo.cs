﻿using System;
using System.Collections.Generic;
using System.Linq;
using UltraNuke.Validation;

namespace UltraNuke.Security
{
    /// <summary>
    /// 用户信息
    /// </summary>
    public class UserInfo
    {
        private Guid id;
        /// <summary>
        /// 用户ID
        /// </summary>
        public virtual Guid Id
        {
			set
            {
                id = value;
            }
            get { return id; }
        }

        private string showName;
        /// <summary>
        /// 显示名称
        /// </summary>
        public virtual string ShowName
        {
            set
            {
                Valid.ValidNullOrEmpty(value, "显示名称不能为空！");
                Valid.ValidStringLength(value, CharacterType.VarChar, 0, 30, "显示名称不能大于30！");

                showName = value;
            }
            get { return showName; }
        }
		
        private string userName;
        /// <summary>
        /// 用户名称
        /// </summary>
        public virtual string UserName
        {
			set
            {
                Valid.ValidNullOrEmpty(value, "用户名称不能为空！");
                Valid.ValidStringLength(value, CharacterType.VarChar, 0, 30, "用户名称不能大于30！");

                userName = value;
            }
            get { return userName; }
        }
		
        private string password;
        /// <summary>
        /// 用户密码
        /// </summary>
        public virtual string Password
        {
			set
            {
                Valid.ValidNullOrEmpty(value, "用户密码不能为空！");
                Valid.ValidStringLength(value, CharacterType.VarChar, 0, 100, "用户密码不能大于100！");

                password = value;
            }
            get { return password; }
        }
		
        private string passwordQuestion;
        /// <summary>
        /// 密码提示问题
        /// </summary>
        public virtual string PasswordQuestion
        {
			set
            {

                Valid.ValidStringLength(value, CharacterType.VarChar, 0, 50, "密码提示问题不能大于50！");

                passwordQuestion = value;
            }
            get { return passwordQuestion; }
        }
		
        private string passwordAnswer;
        /// <summary>
        /// 密码提示回答
        /// </summary>
        public virtual string PasswordAnswer
        {
			set
            {
                Valid.ValidStringLength(value, CharacterType.VarChar, 0, 50, "密码提示回答不能大于50！");

                passwordAnswer = value;
            }
            get { return passwordAnswer; }
        }

        private bool disabled;
        /// <summary>
        /// 停用
        /// </summary>
        public virtual bool Disabled
        {
            set
            {
                disabled = value;
            }
            get { return disabled; }
        }
		
        private int sortNO;
        /// <summary>
        /// 排序
        /// </summary>
        public virtual int SortNO
        {
			set
            {
                sortNO = value;
            }
            get { return sortNO; }
        }

        /// <summary>
        /// 岗位集合显示名称
        /// </summary>
        public virtual string ShowPosts
        {
            get
            {
                if (posts == null)
                    return string.Empty;

                return string.Join(",", posts.Select(o => o.Name).ToArray());
            }
        }

        private IList<PostInfo> posts;
        /// <summary>
        /// 岗位集合
        /// </summary>
        public virtual IList<PostInfo> Posts
        {
            set { posts = value; }
            get
            {
                if (posts == null)
                    posts = new List<PostInfo>();

                return posts;
            }
        }
        
        /// <summary>
        /// 角色集合显示名称
        /// </summary>
        public virtual string ShowRoles
        {
            get
            {
                if (roles == null)
                    return string.Empty;

                return string.Join(",", roles.Select(o => o.Name).ToArray());
            }
        }

        private IList<RoleInfo> roles;
        /// <summary>
        /// 角色集合
        /// </summary>
        public virtual IList<RoleInfo> Roles
        {
            set { roles = value; }
            get
            {
                if (roles == null)
                    roles = new List<RoleInfo>();

                return roles;
            }
        }

        #region 字段
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion
		
    }
}


