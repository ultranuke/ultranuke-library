﻿using System;
using System.Collections.Generic;
using UltraNuke.Validation;

namespace UltraNuke.Security
{
    /// <summary>
    /// 功能信息
    /// </summary>
    public class FunctionInfo
    {
        private Guid id;
        /// <summary>
        /// 功能ID
        /// </summary>
        public virtual Guid Id
        {
			set
            {
                id = value;
            }
            get { return id; }
        }

        private FunctionCategoryInfo category;
        /// <summary>
        /// 功能类别
        /// </summary>
        public virtual FunctionCategoryInfo Category
        {
			set
            {
                category = value;
            }
            get { return category; }
        }
		
        private string name;
        /// <summary>
        /// 功能名称
        /// </summary>
        public virtual string Name
        {
			set
            {
                Valid.ValidNullOrEmpty(value, "功能名称不能为空！");
                Valid.ValidStringLength(value, CharacterType.NVarChar, 0, 20, "功能名称不能大于20！");

                name = value;
            }
            get { return name; }
        }
		
        private int _sortNO;
        /// <summary>
        /// 排序
        /// </summary>
        public virtual int SortNO
        {
			set
            {
                _sortNO = value;
            }
            get { return _sortNO; }
        }

        private IList<RoleInfo> roles;
        /// <summary>
        /// 角色集合
        /// </summary>
        public virtual IList<RoleInfo> Roles
        {
            set { roles = value; }
            get
            {
                if (roles == null)
                    roles = new List<RoleInfo>();

                return roles;
            }
        }

        #region 字段
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion
		
    }
}


