﻿using System;
using System.Collections.Generic;
using UltraNuke.Validation;

namespace UltraNuke.Security
{
    /// <summary>
    /// 部门信息
    /// </summary>
    public class DeptInfo
    {
        private Guid id;
        /// <summary>
        /// 部门ID
        /// </summary>
        public virtual Guid Id
        {
			set
            {
                id = value;
            }
            get { return id; }
        }

        private OrganizationInfo organization;
        /// <summary>
        /// 机构
        /// </summary>
        public virtual OrganizationInfo Organization
        {
			set
            {
                organization = value;
            }
            get { return organization; }
        }
		
        private string deptNo;
        /// <summary>
        /// 部门编号
        /// </summary>
        public virtual string DeptNo
        {
			set
            {
                Valid.ValidNullOrEmpty(value, "部门编号不能为空！");
                Valid.ValidStringLength(value, CharacterType.NVarChar, 0, 50, "部门编号不能大于50！");

                deptNo = value;
            }
            get { return deptNo; }
        }
		
        private string name;
        /// <summary>
        /// 部门名称
        /// </summary>
        public virtual string Name
        {
			set
            {
                Valid.ValidNullOrEmpty(value, "部门名称不能为空！");
                Valid.ValidStringLength(value, CharacterType.NVarChar, 0, 20, "部门名称不能大于20！");

                name = value;
            }
            get { return name; }
        }
		
        private int _sortNO;
        /// <summary>
        /// 排序
        /// </summary>
        public virtual int SortNO
        {
			set
            {
                _sortNO = value;
            }
            get { return _sortNO; }
        }

        private IList<PostInfo> _posts;
        /// <summary>
        /// 岗位集合
        /// </summary>
        public virtual IList<PostInfo> Posts
        {
            set { _posts = value; }
            get
            {
                if (_posts == null)
                    _posts = new List<PostInfo>();

                return _posts;
            }
        }

        #region 字段
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion
		
    }
}


