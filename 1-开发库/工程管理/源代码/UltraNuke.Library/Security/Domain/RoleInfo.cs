﻿using System;
using System.Collections.Generic;
using UltraNuke.Validation;

namespace UltraNuke.Security
{
    /// <summary>
    /// 角色信息
    /// </summary>
    public class RoleInfo
    {
        private Guid id;
        /// <summary>
        /// 角色ID
        /// </summary>
        public virtual Guid Id
        {
			set
            {
                id = value;
            }
            get { return id; }
        }

        private OrganizationInfo organization;
        /// <summary>
        /// 机构信息
        /// </summary>
        public virtual OrganizationInfo Organization
        {
            set
            {
                organization = value;
            }
            get { return organization; }
        }
		
        private string name;
        /// <summary>
        /// 角色名称
        /// </summary>
        public virtual string Name
        {
			set
            {
                Valid.ValidNullOrEmpty(value, "角色名称不能为空！");
                Valid.ValidStringLength(value, CharacterType.NVarChar, 0, 20, "角色名称不能大于20！");

                name = value;
            }
            get { return name; }
        }
		
        private int sortNO;
        /// <summary>
        /// 排序
        /// </summary>
        public virtual int SortNO
        {
			set
            {
                sortNO = value;
            }
            get { return sortNO; }
        }

        private IList<UserInfo> users;
        /// <summary>
        /// 用户集合
        /// </summary>
        public virtual IList<UserInfo> Users
        {
            set { users = value; }
            get
            {
                if (users == null)
                    users = new List<UserInfo>();

                return users;
            }
        }

        private IList<FunctionInfo> functions;
        /// <summary>
        /// 功能集合
        /// </summary>
        public virtual IList<FunctionInfo> Functions
        {
            set { functions = value; }
            get
            {
                if (functions == null)
                    functions = new List<FunctionInfo>();

                return functions;
            }
        }

        #region 字段
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion
    }
}


