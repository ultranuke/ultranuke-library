﻿using System;
using System.Collections.Generic;
using UltraNuke.Validation;

namespace UltraNuke.Security
{
    /// <summary>
    /// 功能类别信息
    /// </summary>
    public class FunctionCategoryInfo
    {
        private Guid id;
        /// <summary>
        /// 功能类别ID
        /// </summary>
        public virtual Guid Id
        {
			set
            {
                id = value;
            }
            get { return id; }
        }
		
        private string name;
        /// <summary>
        /// 功能类别名称
        /// </summary>
        public virtual string Name
        {
			set
            {
                Valid.ValidNullOrEmpty(value, "功能类别名称不能为空！");
                Valid.ValidStringLength(value, CharacterType.NVarChar, 0, 20, "功能类别名称不能大于20！");

                name = value;
            }
            get { return name; }
        }
		
        private int _sortNO;
        /// <summary>
        /// 排序
        /// </summary>
        public virtual int SortNO
        {
			set
            {
                _sortNO = value;
            }
            get { return _sortNO; }
        }

        private IList<PostInfo> _posts;
        /// <summary>
        /// 岗位集合
        /// </summary>
        public virtual IList<PostInfo> Posts
        {
            set { _posts = value; }
            get
            {
                if (_posts == null)
                    _posts = new List<PostInfo>();

                return _posts;
            }
        }

        #region 字段
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion
		
    }
}


