﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UltraNuke.Validation;

namespace UltraNuke.Security
{
    /// <summary>
    /// 岗位信息
    /// </summary>
    public class PostInfo
    {
        private Guid id;
        /// <summary>
        /// 岗位ID
        /// </summary>
        public virtual Guid Id
        {
            set
            {
                id = value;
            }
            get { return id; }
        }

        private DeptInfo dept;
        /// <summary>
        /// 部门
        /// </summary>
        public virtual DeptInfo Dept
        {
            set
            {
                dept = value;
            }
            get { return dept; }
        }



        private string postNo;
        /// <summary>
        /// 岗位编号
        /// </summary>
        public virtual string PostNo
        {
            set
            {
                Valid.ValidNullOrEmpty(value, "岗位编号不能为空！");
                Valid.ValidStringLength(value, CharacterType.NVarChar, 0, 50, "岗位编号不能大于50！");

                postNo = value;
            }
            get { return postNo; }
        }

        private string name;
        /// <summary>
        /// 岗位名称
        /// </summary>
        public virtual string Name
        {
            set
            {
                Valid.ValidNullOrEmpty(value, "岗位名称不能为空！");
                Valid.ValidStringLength(value, CharacterType.NVarChar, 0, 20, "岗位名称不能大于20！");

                name = value;
            }
            get { return name; }
        }

        private int _sortNO;
        /// <summary>
        /// 排序
        /// </summary>
        public virtual int SortNO
        {
            set
            {
                _sortNO = value;
            }
            get { return _sortNO; }
        }

        private IList<UserInfo> users;
        /// <summary>
        /// 用户集合
        /// </summary>
        public virtual IList<UserInfo> Users
        {
            set { users = value; }
            get
            {
                if (users == null)
                    users = new List<UserInfo>();

                return users;
            }
        }

        #region 字段
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion
    }
}
