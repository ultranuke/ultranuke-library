﻿using System;
using System.Collections.Generic;
using UltraNuke.Validation;

namespace UltraNuke.Security
{
    /// <summary>
    /// 机构信息
    /// </summary>
    public class OrganizationInfo
    {
        private Guid id;
        /// <summary>
        /// 机构ID
        /// </summary>
        public virtual Guid Id
        {
			set
            {
                id = value;
            }
            get { return id; }
        }
		
        private string name;
        /// <summary>
        /// 机构名称
        /// </summary>
        public virtual string Name
        {
			set
            {
                Valid.ValidNullOrEmpty(value, "机构名称不能为空！");
                Valid.ValidStringLength(value, CharacterType.NVarChar, 0, 200, "机构简介不能大于200！");

                name = value;
            }
            get { return name; }
        }
		
        private string introduction;
        /// <summary>
        /// 机构简介
        /// </summary>
        public virtual string Introduction
        {
			set
            {
                Valid.ValidStringLength(value, CharacterType.NVarChar, 0, 500, "机构简介不能大于500！");

                introduction = value;
            }
            get { return introduction; }
        }
		
        private string address;
        /// <summary>
        /// 机构地址
        /// </summary>
        public virtual string Address
        {
			set
            {
                Valid.ValidStringLength(value, CharacterType.NVarChar, 0, 200, "机构地址不能大于200！");

                address = value;
            }
            get { return address; }
        }
		
        private string zipcode;
        /// <summary>
        /// 邮政编码
        /// </summary>
        public virtual string Zipcode
        {
			set
            {
                Valid.ValidStringLength(value, CharacterType.VarChar, 0, 6, "邮政编码不能大于6！");

                zipcode = value;
            }
            get { return zipcode; }
        }

        private string contact;
        /// <summary>
        /// 联系人
        /// </summary>
        public virtual string Contact
        {
            set
            {
                Valid.ValidStringLength(value, CharacterType.NVarChar, 0, 50, "联系人不能大于50！");

                contact = value;
            }
            get { return contact; }
        }
		
        private string tel;
        /// <summary>
        /// 电话
        /// </summary>
        public virtual string Tel
        {
			set
            {
                Valid.ValidStringLength(value, CharacterType.VarChar, 0, 50, "电话不能大于50！");

                tel = value;
            }
            get { return tel; }
        }
		
        private string fax;
        /// <summary>
        /// 传真
        /// </summary>
        public virtual string Fax
        {
			set
            {
                Valid.ValidStringLength(value, CharacterType.VarChar, 0, 50, "传真不能大于50！");

                fax = value;
            }
            get { return fax; }
        }
		
        private string email;
        /// <summary>
        /// 电子邮件
        /// </summary>
        public virtual string Email
        {
			set
            {
                Valid.ValidStringLength(value, CharacterType.VarChar, 0, 100, "电子邮件不能大于100！");

                email = value;
            }
            get { return email; }
        }
		
        private string website;
        /// <summary>
        /// 网址
        /// </summary>
        public virtual string Website
        {
			set
            {
                Valid.ValidStringLength(value, CharacterType.VarChar, 0, 100, "网址不能大于100！");

                website = value;
            }
            get { return website; }
        }
		
        private int sortNO;
        /// <summary>
        /// 排序
        /// </summary>
        public virtual int SortNO
        {
			set
            {
                sortNO = value;
            }
            get { return sortNO; }
        }

        private IList<DeptInfo> depts;
        /// <summary>
        /// 部门集合
        /// </summary>
        public virtual IList<DeptInfo> Depts
        {
            set { depts = value; }
            get
            {
                if (depts == null)
                    depts = new List<DeptInfo>();

                return depts;
            }
        }

        private IList<RoleInfo> roles;
        /// <summary>
        /// 角色集合
        /// </summary>
        public virtual IList<RoleInfo> Roles
        {
            set { roles = value; }
            get
            {
                if (roles == null)
                    roles = new List<RoleInfo>();

                return roles;
            }
        }

        #region 字段
        private readonly static IValid Valid = ValidFactory.Create();
        #endregion
		
    }
}


