﻿using UltraNuke.Data;

namespace UltraNuke.Security
{
	/// <summary>
    /// 提供用于功能类别的管理功能
    /// </summary>
    public class FunctionCategoryManager : Data<FunctionCategoryInfo>, IFunctionCategoryManager
    {
    }
}


