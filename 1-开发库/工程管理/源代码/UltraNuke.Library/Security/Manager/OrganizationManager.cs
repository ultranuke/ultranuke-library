﻿using UltraNuke.Data;

namespace UltraNuke.Security
{
	/// <summary>
    /// 提供用于机构的管理功能
    /// </summary>
    public class OrganizationManager : Data<OrganizationInfo>, IOrganizationManager
    {
    }
}


