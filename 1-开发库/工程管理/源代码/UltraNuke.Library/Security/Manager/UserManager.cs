﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UltraNuke.Cryptography;
using UltraNuke.Data;

namespace UltraNuke.Security
{
	/// <summary>
    /// 提供用于用户的管理功能
    /// </summary>
    public class UserManager : Data<UserInfo>, IUserManager
    {
        /// <summary>
        /// 获取用户的信息
        /// </summary>
        /// <param name="userName">用户名称</param>
        /// <returns>用户信息</returns>
        public UserInfo GetUserByUserName(string userName)
        {
            ISession session = NHibernateUtils.Session;
            IList<UserInfo> list = session.CreateCriteria(typeof(UserInfo))
                .Add(Restrictions.Like("UserName", userName))
                .List<UserInfo>();

            if (list.Count > 0)
            {
                return list[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 验证提供的用户名和密码是有效的
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <returns>如果提供的用户名和密码有效，则返回 true；否则返回 false。</returns>
        public bool ValidateUser(string userName, string password)
        {
            password = Crypto.Encrypt(password, CryptoMode.MD5);

            ISession session = NHibernateUtils.Session;
            IList<UserInfo> list = session.CreateCriteria(typeof(UserInfo))
                .Add(Restrictions.Like("UserName", userName))
                .Add(Restrictions.Like("Password", password))
                .List<UserInfo>();

            if (list.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 修改用户的密码
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="newPassword">新密码</param>
        public void UpdatePassword(string userName, string newPassword)
        {

            ISession session = NHibernateUtils.Session;
            IList<UserInfo> list = session.CreateCriteria(typeof(UserInfo))
                .Add(Restrictions.Like("UserName", userName))
                .List<UserInfo>();

            if (list.Count > 0)
            {
                UserInfo userInfo = list[0];
                userInfo.Password = Crypto.Encrypt(newPassword, CryptoMode.MD5);
                session.Update(userInfo);
                session.Flush();
            }
            else
            {
                throw new SecurityException("用户名输入不正确！");
            }
        }

        /// <summary>
        /// 修改用户的密码
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="newPassword">新密码</param>
        public void UpdatePassword(string userName, string password, string newPassword)
        {
            password = Crypto.Encrypt(password, CryptoMode.MD5);

            ISession session = NHibernateUtils.Session;
            IList<UserInfo> list = session.CreateCriteria(typeof(UserInfo))
                .Add(Restrictions.Like("UserName", userName))
                .Add(Restrictions.Like("Password", password))
                .List<UserInfo>();

            if (list.Count > 0)
            {
                UserInfo userInfo = list[0];
                userInfo.Password = Crypto.Encrypt(newPassword, CryptoMode.MD5);
                session.Update(userInfo);
                session.Flush();
            }
            else
            {
                throw new SecurityException("用户名或用户密码输入不正确！");
            }
        }

        /// <summary>
        /// 重置用户的密码
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="newPassword">重置后密码</param>
        /// <returns>如果重置用户的密码成功，则返回 true；否则返回 false。</returns>
        public bool ResetPassword(string userName, out string newPassword)
        {
            Random seed = new Random(10000);
            string sourcePassword = seed.Next().ToString();
            newPassword = Crypto.Encrypt(sourcePassword, CryptoMode.MD5).Substring(0, 10);

            ISession session = NHibernateUtils.Session;
            IList<UserInfo> list = session.CreateCriteria(typeof(UserInfo))
                .Add(Restrictions.Like("UserName", userName))
                .List<UserInfo>();

            if (list.Count > 0)
            {
                UserInfo userInfo = list[0];
                userInfo.Password = Crypto.Encrypt(newPassword, CryptoMode.MD5);
                session.Update(userInfo);
                session.Flush();

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 停用用户
        /// </summary>
        /// <param name="userName">用户名</param>
        public void DisableUser(string userName)
        {
            ISession session = NHibernateUtils.Session;
            IList<UserInfo> list = session.CreateCriteria(typeof(UserInfo))
                .Add(Restrictions.Like("UserName", userName))
                .List<UserInfo>();

            if (list.Count > 0)
            {
                UserInfo userInfo = list[0];
                userInfo.Disabled = true;
                session.Update(userInfo);
                session.Flush();
            }
            else
            {
                throw new SecurityException("没有找到用户名，请检查！");
            }
        }

        /// <summary>
        /// 启用用户
        /// </summary>
        /// <param name="userName">用户名</param>
        public void EnableUser(string userName)
        {
            ISession session = NHibernateUtils.Session;
            IList<UserInfo> list = session.CreateCriteria(typeof(UserInfo))
                .Add(Restrictions.Like("UserName", userName))
                .List<UserInfo>();

            if (list.Count > 0)
            {
                UserInfo userInfo = list[0];
                userInfo.Disabled = false;
                session.Update(userInfo);
                session.Flush();
            }
            else
            {
                throw new SecurityException("没有找到用户名，请检查！");
            }
        }

        #region 字段
        private readonly static ICrypto Crypto = CryptoFactory.Create();
        #endregion
    }
}


