﻿using System.Collections.Generic;
using NHibernate;
using UltraNuke.Data;
using NHibernate.Criterion;

namespace UltraNuke.Security
{
	/// <summary>
    /// 提供用于角色的管理功能
    /// </summary>
    public class RoleManager : Data<RoleInfo>, IRoleManager
    {
        /// <summary>
        /// 获取指定机构的所有角色集合。
        /// </summary>
        /// <param name="org">指定的店铺</param>
        /// <returns>角色的集合</returns>
        public IList<RoleInfo> FindRoles(OrganizationInfo org)
        {
            ISession session = NHibernateUtils.Session;
            try
            {
                IList<RoleInfo> list = session.CreateCriteria(typeof(RoleInfo))
                    .Add(Restrictions.Eq("Organization", org))
                    .AddOrder(Order.Asc("SortNO"))
                    .List<RoleInfo>();

                return list;
            }
            catch (HibernateException ex)
            {
                throw new SecurityException("获取指定机构的所有角色集合发生错误！", ex);
            }
            finally
            {
                //NHibernateUtils.CloseSession();
            }
        }

        /// <summary>
        /// 将指定的用户添加到指定的角色中。
        /// </summary>
        /// <param name="userInfo">用户信息</param>
        /// <param name="roleInfo">角色信息</param>
        public void AddUserToRole(UserInfo userInfo, RoleInfo roleInfo)
        {
            userInfo.Roles.Add(roleInfo);
            roleInfo.Users.Add(userInfo);

            ISession session = NHibernateUtils.Session;
            ITransaction tx = null;
            try
            {
                tx = session.BeginTransaction();

                session.Update(roleInfo);

                tx.Commit();
            }
            catch (HibernateException ex)
            {
                if (tx != null) tx.Rollback();
                throw ex;
            }
            finally
            {
                NHibernateUtils.CloseSession();
            }
        }

        /// <summary>
        /// 从指定的角色中移除指定的用户。
        /// </summary>
        /// <param name="userInfo">用户信息</param>
        /// <param name="roleInfo">角色信息</param>
        public void RemoveUserFromRole(UserInfo userInfo, RoleInfo roleInfo)
        {
            userInfo.Roles.Remove(roleInfo);
            roleInfo.Users.Remove(userInfo);

            ISession session = NHibernateUtils.Session;
            ITransaction tx = null;
            try
            {
                tx = session.BeginTransaction();

                session.Update(roleInfo);

                tx.Commit();
            }
            catch (HibernateException ex)
            {
                if (tx != null) tx.Rollback();
                throw ex;
            }
            finally
            {
                NHibernateUtils.CloseSession();
            }
        }

        /// <summary>
        /// 获取一个用户所属角色的列表
        /// </summary>
        /// <param name="userInfo">用户信息</param>
        /// <returns>角色的集合</returns>
        public IList<RoleInfo> GetRolesForUser(UserInfo userInfo)
        {
            return userInfo.Roles;
        }

        /// <summary>
        /// 获取一个角色所有用户的列表
        /// </summary>
        /// <param name="roleInfo">角色信息</param>
        /// <returns>用户的集合</returns>
        public IList<UserInfo> FindUsersInRole(RoleInfo roleInfo)
        {
            return roleInfo.Users;
        }
    }
}


