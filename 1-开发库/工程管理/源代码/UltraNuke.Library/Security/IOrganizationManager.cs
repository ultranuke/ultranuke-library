﻿using System;
using UltraNuke.Data;

namespace UltraNuke.Security
{
    /// <summary>
    /// 提供用于机构的管理功能接口
    /// </summary>
    public interface IOrganizationManager : IData<OrganizationInfo>
    {
    }
}


