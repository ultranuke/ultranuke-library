﻿using System.Collections.Generic;
using System;
using UltraNuke.Data;

namespace UltraNuke.Security
{
    /// <summary>
    /// 提供用于岗位的管理功能接口
    /// </summary>
    public interface IPostManager : IData<PostInfo>
    {
    }
}


