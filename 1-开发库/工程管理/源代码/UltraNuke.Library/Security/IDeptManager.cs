﻿using System;
using UltraNuke.Data;

namespace UltraNuke.Security
{
    /// <summary>
    /// 提供用于部门的管理功能接口
    /// </summary>
    public interface IDeptManager : IData<DeptInfo>
    {
    }
}


