﻿using System;
using UltraNuke.Data;

namespace UltraNuke.Security
{
    /// <summary>
    /// 提供用于用户的管理功能接口
    /// </summary>
    public interface IUserManager : IData<UserInfo>
    {
        /// <summary>
        /// 获取用户的信息
        /// </summary>
        /// <param name="username">用户名称</param>
        /// <returns>用户信息</returns>
        UserInfo GetUserByUserName(string userName);

        /// <summary>
        /// 验证提供的用户名和密码是有效的
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <returns>如果提供的用户名和密码有效，则返回 true；否则返回 false。</returns>
        bool ValidateUser(string userName, string password);

        /// <summary>
        /// 修改用户的密码
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="newPassword">新密码</param>
        void UpdatePassword(string userName, string newPassword);

        /// <summary>
        /// 修改用户的密码
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="newPassword">新密码</param>
        void UpdatePassword(string userName, string password, string newPassword);

        /// <summary>
        /// 重置用户的密码
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="newPassword">重置后密码</param>
        /// <returns>如果重置用户的密码成功，则返回 true；否则返回 false。</returns>
        bool ResetPassword(string userName, out string newPassword);
        
        /// <summary>
        /// 停用用户
        /// </summary>
        /// <param name="userName">用户名</param>
        void DisableUser(string userName);


        /// <summary>
        /// 启用用户
        /// </summary>
        /// <param name="userName">用户名</param>
        void EnableUser(string userName);
    }
}


