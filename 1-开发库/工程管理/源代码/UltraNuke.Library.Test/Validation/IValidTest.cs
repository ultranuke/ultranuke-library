﻿using UltraNuke.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace UltraNuke.Library.Test
{
    
    
    /// <summary>
    ///这是 IValidTest 的测试类，旨在
    ///包含所有 IValidTest 单元测试
    ///</summary>
    [TestClass()]
    public class IValidTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试属性
        // 
        //编写测试时，还可使用以下属性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///ValidIP 的测试
        ///</summary>
        [TestMethod()]
        public void ValidIPTest()
        {
            IValid target = CreateIValid();
            string input = "127.0.0.1";
            target.ValidIP(input);
        }

        /// <summary>
        ///ValidIP 的测试
        ///</summary>
        [TestMethod()]
        public void ValidIPWithThrowTest()
        {
            IValid target = CreateIValid();
            string input = "127.0.0.1";
            string message = "IP格式不符合要求！";
            target.ValidIP(input, message);
        }


        /// <summary>
        ///ValidUrl 的测试
        ///</summary>
        [TestMethod()]
        public void ValidUrlTest()
        {
            IValid target = CreateIValid();
            string input = "http://www.baidu.com";
            target.ValidUrl(input);
        }

        /// <summary>
        ///ValidUrl 的测试
        ///</summary>
        [TestMethod()]
        public void ValidUrlWithThrowTest()
        {
            IValid target = CreateIValid();
            string input = "http://www.baidu.com";
            string message = "网址格式不符合要求！";
            target.ValidUrl(input, message);
        }

        /// <summary>
        ///ValidTel 的测试
        ///</summary>
        [TestMethod()]
        public void ValidTelTest()
        {
            IValid target = CreateIValid();
            string input = "13112342234";
            target.ValidTel(input);
        }

        /// <summary>
        ///ValidTel 的测试
        ///</summary>
        [TestMethod()]
        public void ValidTelWithThrowTest()
        {
            IValid target = CreateIValid();
            string input = "13112342234";
            string message = "电话格式不符合要求！";
            target.ValidTel(input, message);
        }

        /// <summary>
        ///ValidStringLength 的测试
        ///</summary>
        [TestMethod()]
        public void ValidStringLengthTest2()
        {
            IValid target = CreateIValid();
            string input = "12345";
            int lowerBound = 0;
            int upperBound = 5;
            string message = "字符长度不符合要求！";
            target.ValidStringLength(input, CharacterType.VarChar, lowerBound, upperBound, message);
        }

        /// <summary>
        ///ValidStringLength 的测试
        ///</summary>
        [TestMethod()]
        public void ValidStringLengthTest1()
        {
            IValid target = CreateIValid();
            string input = "12345";
            int upperBound = 5;
            target.ValidStringLength(input, CharacterType.VarChar, upperBound);
        }

        /// <summary>
        ///ValidStringLength 的测试
        ///</summary>
        [TestMethod()]
        public void ValidStringLengthTest0()
        {
            IValid target = CreateIValid();
            string input = "12345";
            int lowerBound = 0;
            int upperBound = 5;
            target.ValidStringLength(input, CharacterType.VarChar, lowerBound, upperBound);
        }

        /// <summary>
        ///ValidRegex 的测试
        ///</summary>
        [TestMethod()]
        public void ValidRegexWithThrowTest()
        {
            IValid target = CreateIValid();
            string input = "12345";
            string regex = @"^\d+$";
            string message = "符合数字格式！";
            target.ValidRegex(input, regex, message);
        }

        /// <summary>
        ///ValidPostfix 的测试
        ///</summary>
        [TestMethod()]
        public void ValidPostfixWithThrowTest()
        {
            IValid target = CreateIValid();
            string input = ".jpg";
            string fix = "gif|jpg";
            string message = "不存在后缀名（gif|jpg）！";
            target.ValidPostfix(input, fix, message);
        }

        /// <summary>
        ///ValidPostfix 的测试
        ///</summary>
        [TestMethod()]
        public void ValidPostfixTest()
        {
            IValid target = CreateIValid();
            string input = ".jpg";
            string fix = "gif|jpg";
            target.ValidPostfix(input, fix);
        }

        /// <summary>
        ///ValidNullOrEmpty 的测试
        ///</summary>
        [TestMethod()]
        public void ValidNullOrEmptyWithThrowTest()
        {
            IValid target = CreateIValid();
            string input = "112233";
            string message = "字符不为空！";
            target.ValidNullOrEmpty(input, message);
        }

        /// <summary>
        ///ValidNullOrEmpty 的测试
        ///</summary>
        [TestMethod()]
        public void ValidNullOrEmptyTest()
        {
            IValid target = CreateIValid();
            string input = "112233";
            target.ValidNullOrEmpty(input);
        }

        /// <summary>
        ///ValidEmail 的测试
        ///</summary>
        [TestMethod()]
        public void ValidEmailWithThrowTest()
        {
            IValid target = CreateIValid();
            string input = "1000@qq.com";
            string message = "电子邮件格式不符合要求！";
            target.ValidEmail(input, message);
        }

        /// <summary>
        ///ValidEmail 的测试
        ///</summary>
        [TestMethod()]
        public void ValidEmailTest()
        {
            IValid target = CreateIValid();
            string input = "1000@qq.com";
            target.ValidEmail(input);
        }

        internal virtual IValid CreateIValid()
        {
            IValid target = ValidFactory.Create();
            return target;
        }
    }
}
