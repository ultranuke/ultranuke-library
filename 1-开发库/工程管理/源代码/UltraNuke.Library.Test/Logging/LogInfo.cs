﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UltraNuke.Library.Test
{
    public class LogInfo
    {
        public LogInfo()
        {
        }

        public LogInfo(int logId, DateTime date, string level, string message, string exception)
        {
            LogId = logId;
            Date = date;
            Level = level;
            Message = message;
            Exception = exception;
        }

        private int _logId;
        public virtual int LogId
        {
            set { _logId = value; }
            get { return _logId; }
        }

        private DateTime _date;
        public virtual DateTime Date
        {
            set { _date = value; }
            get { return _date; }
        }

        private string _level;
        public virtual string Level
        {
            set { _level = value; }
            get { return _level; }
        }

        private string _message;
        public virtual string Message
        {
            set { _message = value; }
            get { return _message; }
        }

        private string _exception;
        public virtual string Exception
        {
            set { _exception = value; }
            get { return _exception; }
        }
    }
}
