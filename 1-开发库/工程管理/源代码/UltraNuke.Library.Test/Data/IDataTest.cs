﻿using UltraNuke.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System;

namespace UltraNuke.Library.Test
{
    
    
    /// <summary>
    ///这是 IDataTest 的测试类，旨在
    ///包含所有 IDataTest 单元测试
    ///</summary>
    [TestClass()]
    public class IDataTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试属性
        // 
        //编写测试时，还可使用以下属性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Save 的测试
        ///</summary>
        [TestMethod()]
        public void SaveTest()
        {
            IData target = CreateIData();
            LogInfo obj = new LogInfo(1,DateTime.Now,"Debug","错误！","错误！");
            int actual = 0;
            try
            {
                target.BeginTransaction();

                actual = (int)target.Save(obj);

                target.Commit();
            }
            catch
            {
                target.Rollback();
            }
            finally
            {
                target.Dispose();
            }
            Assert.IsTrue(actual>0);
        }

        /// <summary>
        ///Update 的测试
        ///</summary>
        [TestMethod()]
        public void UpdateTest()
        {
            IData target = CreateIData();
            LogInfo obj = new LogInfo(1, DateTime.Now, "Debug", "错误！", "错误！");
            int actual;
            try
            {
                target.BeginTransaction();

                actual = (int)target.Save(obj);

                LogInfo obj2 = new LogInfo(actual, DateTime.Now, "Debug", "错误UpdateTest！", "错误UpdateTest！");
                target.Clear();
                target.Update(obj2);

                target.Commit();
            }
            catch
            {
                target.Rollback();
            }
            finally
            {
                target.Dispose();
            }
        }

        /// <summary>
        ///Gets 的测试
        ///</summary>
        [TestMethod()]
        public void GetsTest()
        {
            IData target = CreateIData();
            LogInfo obj = new LogInfo(1, DateTime.Now, "Debug", "错误！", "错误！");
            int id;
            try
            {
                target.BeginTransaction();

                id = (int)target.Save(obj);

                target.Commit();
            }
            catch
            {
                target.Rollback();
            }
            finally
            {
                target.Dispose();
            }

            target = CreateIData();
            IList<LogInfo> list = target.Gets<LogInfo>();
            Assert.IsTrue(list.Count > 0);
        }

        /// <summary>
        ///Get 的测试
        ///</summary>
        [TestMethod()]
        public void GetTest()
        {
            IData target = CreateIData();
            LogInfo obj = new LogInfo(1, DateTime.Now, "Debug", "错误！", "错误！");
            int id = -1;
            try
            {
                target.BeginTransaction();

                id = (int)target.Save(obj);

                target.Commit();
            }
            catch
            {
                target.Rollback();
            }
            finally
            {
                target.Dispose();
            }

            target = CreateIData();
            LogInfo obj2 = target.Get<LogInfo>(id);
            Assert.AreEqual(obj2.LogId, id);
        }

        /// <summary>
        ///Delete 的测试
        ///</summary>
        [TestMethod()]
        public void DeleteTest()
        {
            IData target = CreateIData();
            LogInfo obj = new LogInfo(1, DateTime.Now, "Debug", "错误！", "错误！");
            int actual;
            try
            {
                target.BeginTransaction();

                actual = (int)target.Save(obj);

                LogInfo obj2 = new LogInfo(actual, DateTime.Now, "Debug", "错误UpdateTest！", "错误UpdateTest！");
                target.Clear();
                target.Delete(obj2);

                target.Commit();
            }
            catch
            {
                target.Rollback();
            }
            finally
            {
                target.Dispose();
            }
        }

        internal virtual IData CreateIData()
        {
            IData target = DataFactory.Create();
            return target;
        }
    }
}
