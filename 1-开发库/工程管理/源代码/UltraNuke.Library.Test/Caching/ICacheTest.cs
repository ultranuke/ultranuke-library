﻿using UltraNuke.Caching;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace UltraNuke.Library.Test
{
    
    
    /// <summary>
    ///这是 ICacheTest 的测试类，旨在
    ///包含所有 ICacheTest 单元测试
    ///</summary>
    [TestClass()]
    public class ICacheTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试属性
        // 
        //编写测试时，还可使用以下属性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///Add 的测试
        ///</summary>
        [TestMethod()]
        public void AddTest()
        {
            ICache target = CreateICache(); 
            string key = "UserName";
            object value = "TestUser1";
            target.Add(key, value);
        }

        /// <summary>
        ///Contains 的测试
        ///</summary>
        [TestMethod()]
        public void ContainsTest()
        {
            ICache target = CreateICache();
            string key = "UserName";
            object value = "TestUser2";
            target.Add(key, value);

            bool expected = true;
            bool actual;
            actual = target.Contains(key);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Flush 的测试
        ///</summary>
        [TestMethod()]
        public void FlushTest()
        {
            ICache target = CreateICache();
            string key = "UserName";
            object value = "TestUser2";
            target.Add(key, value);

            target.Flush();
            Assert.IsTrue(target.Count == 0);
        }

        /// <summary>
        ///GetData 的测试
        ///</summary>
        [TestMethod()]
        public void GetDataTest()
        {
            ICache target = CreateICache();
            string key = "UserName";
            object value = "TestUser2";
            target.Add(key, value);

            object expected = value;
            object actual = "TestUser2";
            actual = target.GetData(key);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Remove 的测试
        ///</summary>
        [TestMethod()]
        public void RemoveTest()
        {
            ICache target = CreateICache();
            string key = "UserName";
            object value = "TestUser2";
            target.Add(key, value);
            Assert.IsTrue(target.Contains(key));
            target.Remove(key);
            Assert.IsFalse(target.Contains(key));
        }

        /// <summary>
        ///Count 的测试
        ///</summary>
        [TestMethod()]
        public void CountTest()
        {
            ICache target = CreateICache();
            target.Flush();
            string key = "UserName";
            object value = "TestUser2";
            target.Add(key, value);
            int actual;
            actual = target.Count;
            Assert.AreEqual(1,actual);
        }

        /// <summary>
        ///Item 的测试
        ///</summary>
        [TestMethod()]
        public void ItemTest()
        {
            ICache target = CreateICache();
            string key = "UserName";
            object value = "TestUser2";
            target.Add(key, value);

            object actual;
            actual = target[key];

            Assert.AreEqual("TestUser2", actual);
        }

        internal virtual ICache CreateICache()
        {
            ICache target = CacheFactory.Create();
            return target;
        }
    }
}
