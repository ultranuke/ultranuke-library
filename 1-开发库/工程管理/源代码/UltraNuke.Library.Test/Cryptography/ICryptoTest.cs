﻿using UltraNuke.Cryptography;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
namespace UltraNuke.Library.Test
{
    
    
    /// <summary>
    ///这是 ICryptoTest 的测试类，旨在
    ///包含所有 ICryptoTest 单元测试
    ///</summary>
    [TestClass()]
    public class ICryptoTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试属性
        // 
        //编写测试时，还可使用以下属性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///EncryptTestByDES 的测试
        ///</summary>
        [TestMethod()]
        public void EncryptTestByDES()
        {
            ICrypto target = CreateICrypto();
            string plainText = "Hello";
            string expected = "8EWXWFK6uLA=";
            string actual;
            actual = target.Encrypt(plainText, CryptoMode.DES);
            Console.Write(actual);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///DecryptTestByDES 的测试
        ///</summary>
        [TestMethod()]
        public void DecryptTestByDES()
        {
            ICrypto target = CreateICrypto();
            string cryptoText = "8EWXWFK6uLA=";
            string expected = "Hello"; 
            string actual;
            actual = target.Decrypt(cryptoText, CryptoMode.DES);
            Console.Write(actual);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///EncryptTestByRC2 的测试
        ///</summary>
        [TestMethod()]
        public void EncryptTestByRC2()
        {
            ICrypto target = CreateICrypto();
            string plainText = "Hello";
            string expected = "f1xSIbKxNS8=";
            string actual;
            actual = target.Encrypt(plainText, CryptoMode.RC2);
            Console.Write(actual);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///DecryptTestByRC2 的测试
        ///</summary>
        [TestMethod()]
        public void DecryptTestByRC2()
        {
            ICrypto target = CreateICrypto();
            string cryptoText = "f1xSIbKxNS8=";
            string expected = "Hello";
            string actual;
            actual = target.Decrypt(cryptoText, CryptoMode.RC2);
            Console.Write(actual);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///EncryptTestByRSA 的测试
        ///</summary>
        [TestMethod()]
        public void EncryptTestByRSA()
        {
            ICrypto target = CreateICrypto();
            string plainText = "Hello";
            string cryptoText = target.Encrypt(plainText, CryptoMode.RSA);
            Console.WriteLine(cryptoText);
            string actual;
            actual = target.Decrypt(cryptoText, CryptoMode.RSA);
            Console.WriteLine(actual);
            Assert.AreEqual(plainText, actual);
        }

        /// <summary>
        ///EncryptTestByMD5 的测试
        ///</summary>
        [TestMethod()]
        public void EncryptTestByMD5()
        {
            ICrypto target = CreateICrypto();
            string plainText = "Hello";
            string expected = "8YMMI8EgFXCbxU5z/uS4qg==";
            string actual;
            actual = target.Encrypt(plainText, CryptoMode.MD5);
            Console.Write(actual);
            Assert.AreEqual(expected, actual);
        }

        internal virtual ICrypto CreateICrypto()
        {
            ICrypto target = CryptoFactory.Create();
            return target;
        }
    }
}
