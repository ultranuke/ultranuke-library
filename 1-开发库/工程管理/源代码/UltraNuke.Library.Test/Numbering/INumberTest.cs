﻿using UltraNuke.Numbering;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
namespace UltraNuke.Library.Test
{
    
    
    /// <summary>
    ///这是 INumberTest 的测试类，旨在
    ///包含所有 INumberTest 单元测试
    ///</summary>
    [TestClass()]
    public class INumberTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试属性
        // 
        //编写测试时，还可使用以下属性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///RecycleNumberTest 的测试
        ///</summary>
        [TestMethod()]
        public void RecycleNumberTest()
        {
            INumber target = NumberFactory.Create(NumberMode.连续编号);
            string name = "受理编号";
            DateTime numberDate = DateTime.Now;
            string regNo = "A000001";
            target.RecycleNumber(name, numberDate,regNo);
        }

        /// <summary>
        ///GenerateNumberTest 的测试
        ///</summary>
        [TestMethod()]
        public void GenerateNumberTest()
        {
            INumber target = NumberFactory.Create(NumberMode.连续编号);
            string name = "受理编号";
            DateTime numberDate = DateTime.Now;
            string expected = "A000001";
            string actual;
            actual = target.GenerateNumber(name, numberDate);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///RecycleNumberByDayTest 的测试
        ///</summary>
        [TestMethod()]
        public void RecycleNumberByDayTest()
        {
            INumber target = NumberFactory.Create(NumberMode.按日连续编号);
            string name = "受理编号";
            DateTime numberDate = DateTime.Now;
            string regNo = "A2012120401";
            target.RecycleNumber(name, numberDate, regNo);
        }

        /// <summary>
        ///GenerateNumberByDayTest 的测试
        ///</summary>
        [TestMethod()]
        public void GenerateNumberByDayTest()
        {
            INumber target = NumberFactory.Create(NumberMode.按日连续编号);
            string name = "受理编号";
            DateTime numberDate = DateTime.Now;
            string expected = "A2012120401";
            string actual;
            actual = target.GenerateNumber(name, numberDate);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///RecycleNumberByMonthTest 的测试
        ///</summary>
        [TestMethod()]
        public void RecycleNumberByMonthTest()
        {
            INumber target = NumberFactory.Create(NumberMode.按月连续编号);
            string name = "受理编号";
            DateTime numberDate = DateTime.Now;
            string regNo = "A2012120001";
            target.RecycleNumber(name, numberDate, regNo);
        }

        /// <summary>
        ///GenerateNumberByMonthTest 的测试
        ///</summary>
        [TestMethod()]
        public void GenerateNumberByMonthTest()
        {
            INumber target = NumberFactory.Create(NumberMode.按月连续编号);
            string name = "受理编号";
            DateTime numberDate = DateTime.Now;
            string expected = "A2012120001";
            string actual;
            actual = target.GenerateNumber(name, numberDate);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///RecycleNumberByYearTest 的测试
        ///</summary>
        [TestMethod()]
        public void RecycleNumberByYearTest()
        {
            INumber target = NumberFactory.Create(NumberMode.按年连续编号);
            string name = "受理编号";
            DateTime numberDate = DateTime.Now;
            string regNo = "A20120001";
            target.RecycleNumber(name, numberDate, regNo);
        }

        /// <summary>
        ///GenerateNumberByYearTest 的测试
        ///</summary>
        [TestMethod()]
        public void GenerateNumberByYearTest()
        {
            INumber target = NumberFactory.Create(NumberMode.按年连续编号);
            string name = "受理编号";
            DateTime numberDate = DateTime.Now;
            string expected = "A20120001";
            string actual;
            actual = target.GenerateNumber(name, numberDate);
            Assert.AreEqual(expected, actual);
        }

        internal virtual INumber CreateINumber()
        {
            INumber target = new NumberManager();
            return target;
        }
    }
}
