﻿using UltraNuke.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UltraNuke.Cryptography;
using System.Diagnostics;
namespace UltraNuke.Library.Test
{
    
    
    /// <summary>
    ///这是 IUserManagerTest 的测试类，旨在
    ///包含所有 IUserManagerTest 单元测试
    ///</summary>
    [TestClass()]
    public class IUserManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试属性
        // 
        //编写测试时，还可使用以下属性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///Crud 的测试
        ///</summary>
        [TestMethod()]
        public void CrudTest()
        {
            UserInfo initData = InitialData();
            IUserManager target;

            target = CreateInstance();
            Guid id = (Guid)target.Save(initData);
            Assert.IsNotNull(id);

            target = CreateInstance();
            IList<UserInfo> list = target.Gets();
            Assert.IsTrue(list.Count > 0);

            target = CreateInstance();
            UserInfo info = target.Get(id);
            Assert.AreEqual(info.UserName, "qq");

            target = CreateInstance();
            info.Id = id;
            info.UserName = "qq（修改）";
            target.Update(info);

            target = CreateInstance();
            UserInfo info2 = target.Get(id);
            Assert.AreEqual(info2.UserName, "qq（修改）");

            target = CreateInstance();
            target.Delete(id);

            target = CreateInstance();
            UserInfo info3 = target.Get(id);
            Assert.IsNull(info3);
        }

        /// <summary>
        ///ValidateUser 的测试
        ///</summary>
        [TestMethod()]
        public void ValidateUserTest()
        {
            IUserManager target = CreateInstance();
            string userName = "lih";
            string password = "1117";
            bool expected = true; 
            bool actual;
            actual = target.ValidateUser(userName, password);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///UpdatePassword 的测试
        ///</summary>
        [TestMethod()]
        public void UpdatePasswordTest()
        {
            IUserManager target = CreateInstance();
            string userName = "lih";
            string password = "1117";
            string newPassword = "1118";
            target.UpdatePassword(userName, password, newPassword);
            UserInfo actual = target.GetUserByUserName(userName);
            Assert.AreEqual(actual.Password, Crypto.Encrypt(newPassword, CryptoMode.MD5));
            
        }

        /// <summary>
        ///ResetPassword 的测试
        ///</summary>
        [TestMethod()]
        public void ResetPasswordTest()
        {
            IUserManager target = CreateInstance();
            string userName = "lih";
            string newPassword;
            bool expected = true;
            bool actual;
            actual = target.ResetPassword(userName, out newPassword);
            Console.Write(newPassword);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///GetUserByUserName 的测试
        ///</summary>
        [TestMethod()]
        public void GetUserByUserNameTest()
        {
            IUserManager target = CreateInstance();
            string userName = "lih";
            UserInfo actual;
            actual = target.GetUserByUserName(userName);
            Assert.AreEqual(userName, actual.UserName);
        }

        /// <summary>
        ///EnableUser 的测试
        ///</summary>
        [TestMethod()]
        public void EnableUserTest()
        {
            IUserManager target = CreateInstance();
            string userName = "lih";
            target.EnableUser(userName);
        }

        /// <summary>
        ///DisableUser 的测试
        ///</summary>
        [TestMethod()]
        public void DisableUserTest()
        {
            IUserManager target = CreateInstance();
            string userName = "lih";
            target.DisableUser(userName);
        }

        ICrypto Crypto = CryptoFactory.Create();
        private UserInfo InitialData()
        {

            UserInfo info = new UserInfo();
            info.ShowName = "QQ";
            info.UserName = "qq";
            info.Password = Crypto.Encrypt("1117", CryptoMode.MD5);
            info.PasswordQuestion = "生日";
            info.PasswordAnswer = "1117";
            info.Disabled = false;
            info.SortNO = 1;
            return info;
        }

        internal virtual IUserManager CreateInstance()
        {
            IUserManager target = new UserManager();
            return target;
        }
    }
}
