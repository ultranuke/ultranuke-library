﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UltraNuke.Security;

namespace UltraNuke.Library.Test
{
    
    
    /// <summary>
    ///这是 IPostManagerTest 的测试类，旨在
    ///包含所有 IPostManagerTest 单元测试
    ///</summary>
    [TestClass()]
    public class IPostManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试属性
        // 
        //编写测试时，还可使用以下属性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///Crud 的测试
        ///</summary>
        [TestMethod()]
        public void CrudTest()
        {
            PostInfo initData = InitialData();
            IPostManager target;

            target = CreateInstance();
            Guid id = (Guid)target.Save(initData);
            Assert.IsNotNull(id);

            target = CreateInstance();
            IList<PostInfo> list = target.Gets();
            Assert.IsTrue(list.Count > 0);

            target = CreateInstance();
            PostInfo info = target.Get(id);
            Assert.AreEqual(info.Name, "营业员");

            target = CreateInstance();
            info.Id = id;
            info.Name = "营业员（修改）";
            target.Update(info);

            target = CreateInstance();
            PostInfo info2 = target.Get(id);
            Assert.AreEqual(info2.Name, "营业员（修改）");

            target = CreateInstance();
            target.Delete(id);

            target = CreateInstance();
            PostInfo info3 = target.Get(id);
            Assert.IsNull(info3);
        }

        private PostInfo InitialData()
        {
            IDeptManager target = new DeptManager();
            DeptInfo dept = target.Get(new Guid("23efb19f-9f75-42d8-a7ea-673995800330"));

            PostInfo info = new PostInfo();
            info.Dept = dept;
            info.PostNo = "1000";
            info.Name = "营业员";
            info.SortNO = 1;
            return info;
        }

        private IPostManager CreateInstance()
        {
            IPostManager target = new PostManager();
            return target;
        }
    }
}
