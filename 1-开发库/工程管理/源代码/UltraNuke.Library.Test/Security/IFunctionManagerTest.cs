﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UltraNuke.Security;

namespace UltraNuke.Library.Test
{
    
    
    /// <summary>
    ///这是 IFunctionManagerTest 的测试类，旨在
    ///包含所有 IFunctionManagerTest 单元测试
    ///</summary>
    [TestClass()]
    public class IFunctionManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试属性
        // 
        //编写测试时，还可使用以下属性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Crud 的测试
        ///</summary>
        [TestMethod()]
        public void CrudTest()
        {
            FunctionInfo initData = InitialData();
            IFunctionManager target;

            target = CreateInstance();
            Guid id = (Guid)target.Save(initData);
            Assert.IsNotNull(id);

            target = CreateInstance();
            IList<FunctionInfo> list = target.Gets();
            Assert.IsTrue(list.Count > 0);

            target = CreateInstance();
            FunctionInfo info = target.Get(id);
            Assert.AreEqual(info.Name, "销售零售");

            target = CreateInstance();
            info.Id = id;
            info.Name = "销售零售（修改）";
            target.Update(info);

            target = CreateInstance();
            FunctionInfo info2 = target.Get(id);
            Assert.AreEqual(info2.Name, "销售零售（修改）");

            target = CreateInstance();
            target.Delete(id);

            target = CreateInstance();
            FunctionInfo info3 = target.Get(id);
            Assert.IsNull(info3);
        }

        private FunctionInfo InitialData()
        {
            IFunctionCategoryManager target = new FunctionCategoryManager();
            FunctionCategoryInfo fc = target.Get(new Guid("a4867ba3-de2e-4899-ac14-2691dc62bd06"));

            FunctionInfo info = new FunctionInfo();
            info.Category = fc;
            info.Name = "销售零售";
            info.SortNO = 1;
            return info;
        }

        private IFunctionManager CreateInstance()
        {
            IFunctionManager target = new FunctionManager();
            return target;
        }
    }
}
