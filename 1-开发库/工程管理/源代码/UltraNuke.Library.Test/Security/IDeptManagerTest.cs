﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UltraNuke.Security;

namespace UltraNuke.Library.Test
{
    
    
    /// <summary>
    ///这是 IDeptManagerTest 的测试类，旨在
    ///包含所有 IDeptManagerTest 单元测试
    ///</summary>
    [TestClass()]
    public class IDeptManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试属性
        // 
        //编写测试时，还可使用以下属性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        /// Crud 的测试
        ///</summary>
        [TestMethod()]
        public void CrudTest()
        {
            DeptInfo initData = InitialData();
            IDeptManager target;

            target = CreateInstance();
            Guid id = (Guid)target.Save(initData);
            Assert.IsNotNull(id);

            target = CreateInstance();
            IList<DeptInfo> list = target.Gets();
            Assert.IsTrue(list.Count>0);

            target = CreateInstance();
            DeptInfo info = target.Get(id);
            Assert.AreEqual(info.Name, "销售部");

            target = CreateInstance();
            info.Id = id;
            info.Name = "销售部（修改）";
            target.Update(info);

            target = CreateInstance();
            DeptInfo info2 = target.Get(id);
            Assert.AreEqual(info2.Name, "销售部（修改）");
            
            target = CreateInstance();
            target.Delete(id);

            target = CreateInstance();
            DeptInfo info3 = target.Get(id);
            Assert.IsNull(info3);
        }

        private DeptInfo InitialData()
        {
            IOrganizationManager target = new OrganizationManager();
            OrganizationInfo org = target.Get(new Guid("ecced713-2966-4f09-acbe-9e9c2bdbd14b"));

            DeptInfo info = new DeptInfo();
            info.Organization = org;
            info.DeptNo = "10000";
            info.Name = "销售部";
            info.SortNO = 1;
            return info;
        }

        private IDeptManager CreateInstance()
        {
            IDeptManager target = new DeptManager();
            return target;
        }
    }
}
