﻿using UltraNuke.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System;

namespace UltraNuke.Library.Test
{
    
    
    /// <summary>
    ///这是 IRoleManagerTest 的测试类，旨在
    ///包含所有 IRoleManagerTest 单元测试
    ///</summary>
    [TestClass()]
    public class IRoleManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试属性
        // 
        //编写测试时，还可使用以下属性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///RemoveUserFromRole 的测试
        ///</summary>
        [TestMethod()]
        public void RemoveUserFromRoleTest()
        {
            IUserManager target2 = new UserManager();
            IRoleManager target = CreateInstance();
            UserInfo userInfo = target2.Get(new Guid("80fea4c3-9356-4761-bfce-9769cee93e75"));
            RoleInfo roleInfo = target.Get(new Guid("0c07d62e-b943-4a6b-9a89-e24bde1b6172"));
            target.RemoveUserFromRole(userInfo, roleInfo);
        }

        /// <summary>
        ///GetRolesForUser 的测试
        ///</summary>
        [TestMethod()]
        public void GetRolesForUserTest()
        {
            IUserManager target2 = new UserManager();
            IRoleManager target = CreateInstance();
            UserInfo userInfo = target2.Get(new Guid("80fea4c3-9356-4761-bfce-9769cee93e75"));
            IList<RoleInfo> actual;
            actual = target.GetRolesForUser(userInfo);
            Assert.IsTrue(actual.Count>0);
        }

        /// <summary>
        ///FindUsersInRole 的测试
        ///</summary>
        [TestMethod()]
        public void FindUsersInRoleTest()
        {
            IRoleManager target = CreateInstance();
            RoleInfo roleInfo = target.Get(new Guid("0c07d62e-b943-4a6b-9a89-e24bde1b6172"));
            IList<UserInfo> actual;
            actual = target.FindUsersInRole(roleInfo);
            Assert.IsTrue(actual.Count>0);
        }

        /// <summary>
        ///AddUserToRole 的测试
        ///</summary>
        [TestMethod()]
        public void AddUserToRoleTest()
        {
            IUserManager target2 = new UserManager();
            IRoleManager target = CreateInstance();
            UserInfo userInfo = target2.Get(new Guid("80fea4c3-9356-4761-bfce-9769cee93e75"));
            RoleInfo roleInfo = target.Get(new Guid("0c07d62e-b943-4a6b-9a89-e24bde1b6172"));
            target.AddUserToRole(userInfo, roleInfo);
        }

        /// <summary>
        ///Crud 的测试
        ///</summary>
        [TestMethod()]
        public void CrudTest()
        {
            RoleInfo initData = InitialData();
            IRoleManager target;

            target = CreateInstance();
            Guid id = (Guid)target.Save(initData);
            Assert.IsNotNull(id);

            target = CreateInstance();
            IList<RoleInfo> list = target.Gets();
            Assert.IsTrue(list.Count > 0);

            target = CreateInstance();
            RoleInfo info = target.Get(id);
            Assert.AreEqual(info.Name, "营业员");

            target = CreateInstance();
            info.Id = id;
            info.Name = "营业员（修改）";
            target.Update(info);

            target = CreateInstance();
            RoleInfo info2 = target.Get(id);
            Assert.AreEqual(info2.Name, "营业员（修改）");

            target = CreateInstance();
            target.Delete(id);

            target = CreateInstance();
            RoleInfo info3 = target.Get(id);
            Assert.IsNull(info3);
        }

        private RoleInfo InitialData()
        {
            IOrganizationManager target = new OrganizationManager();
            OrganizationInfo org = target.Get(new Guid("ecced713-2966-4f09-acbe-9e9c2bdbd14b"));

            RoleInfo info = new RoleInfo();
            info.Organization = org;
            info.Name = "营业员";
            info.SortNO = 1;
            return info;
        }

        internal virtual IRoleManager CreateInstance()
        {
            IRoleManager target = new RoleManager();
            return target;
        }
    }
}
