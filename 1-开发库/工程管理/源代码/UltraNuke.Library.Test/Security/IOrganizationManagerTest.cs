﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UltraNuke.Security;

namespace UltraNuke.Library.Test
{
    
    
    /// <summary>
    ///这是 IOrganizationManagerTest 的测试类，旨在
    ///包含所有 IOrganizationManagerTest 单元测试
    ///</summary>
    [TestClass()]
    public class IOrganizationManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试属性
        // 
        //编写测试时，还可使用以下属性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///Crud 的测试
        ///</summary>
        [TestMethod()]
        public void CrudTest()
        {
            OrganizationInfo initData = InitialData();
            IOrganizationManager target;

            target = CreateInstance();
            Guid id = (Guid)target.Save(initData);
            Assert.IsNotNull(id);

            target = CreateInstance();
            IList<OrganizationInfo> list = target.Gets();
            Assert.IsTrue(list.Count > 0);

            target = CreateInstance();
            OrganizationInfo info = target.Get(id);
            Assert.AreEqual(info.Name, "纽客美食");

            target = CreateInstance();
            info.Id = id;
            info.Name = "纽客美食（修改）";
            target.Update(info);

            target = CreateInstance();
            OrganizationInfo info2 = target.Get(id);
            Assert.AreEqual(info2.Name, "纽客美食（修改）");

            target = CreateInstance();
            target.Delete(id);

            target = CreateInstance();
            OrganizationInfo info3 = target.Get(id);
            Assert.IsNull(info3);
        }

        private OrganizationInfo InitialData()
        {
            OrganizationInfo info = new OrganizationInfo();
            info.Name = "纽客美食";
            info.Introduction = "美食、干货";
            info.Address = "浙江宁波";
            info.Zipcode = "315400";
            info.Contact = "阿里";
            info.Tel = "0574-88886666";
            info.Fax = "0574-88886666";
            info.Email = "qq@ultranuke.com";
            info.Website = "http://www.ultranuke.com/";
            info.SortNO = 1;
            return info;
        }

        private IOrganizationManager CreateInstance()
        {
            IOrganizationManager target = new OrganizationManager();
            return target;
        }
    }
}
